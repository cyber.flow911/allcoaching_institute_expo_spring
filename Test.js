import { View, Text, Button, Dimensions,Image, ScrollView } from 'react-native'
import React, { useState } from 'react'
import { Animated } from 'react-native'
const {height,width} = Dimensions.get('window')
import * as ScreenOrientation from 'expo-screen-orientation'
import { Assets } from './Components/config'

const Test = () => {

    const viewXY = new  Animated.ValueXY({ x: 200, y: 200 })
    const [currentOrientation,setCurrentOrientation]= useState('portrait')


    const rotateWithOrientation = (height,width) => {
        Animated.timing(viewXY, {
            toValue: { x: height, y: width },
            duration: 1000,
            useNativeDriver: false

        }).start(() => setCurrentOrientation(currentOrientation=='portrait'?'landscape':'portrait'))
    }
    return (
        <ScrollView>
            <Animated.View style={{ width: viewXY.x, height: viewXY.y }}>
                <Animated.Image source={Assets.profile.profileIcon}  style={{width:"100%",height:"100%"}}/>
                <Button title="Rotate" onPress={() =>{
                        if(currentOrientation=='portrait'){
                                
                            ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE_RIGHT)
                            rotateWithOrientation(height,width)
                        }else{  
                            ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT)
                            rotateWithOrientation(200,200)
                        } 
                    }} />
            </Animated.View>
        </ScrollView>
    )
}

export default Test