package com.dubuddy.DuBuddyInstitutes.Respository;


import com.dubuddy.DuBuddyInstitutes.Entity.StudentHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentHistoryRepo extends PagingAndSortingRepository<StudentHistory,Long> {


    Page<StudentHistory> findByStudentId(long studentId,Pageable pageable);
    Page<StudentHistory> findByStudentIdAndType(long studentId,String type,Pageable pageable);
}
