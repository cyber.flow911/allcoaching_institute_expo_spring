package com.dubuddy.DuBuddyInstitutes.Respository;

import com.dubuddy.DuBuddyInstitutes.Entity.StudentMessageImages;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentMessageImagesRepo extends PagingAndSortingRepository<StudentMessageImages,Long>
{

}
