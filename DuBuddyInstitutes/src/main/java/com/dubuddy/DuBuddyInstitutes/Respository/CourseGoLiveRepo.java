package com.dubuddy.DuBuddyInstitutes.Respository;

import com.dubuddy.DuBuddyInstitutes.Entity.CourseGoLive;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CourseGoLiveRepo extends PagingAndSortingRepository<CourseGoLive,Long> {

    Iterable<CourseGoLive> findByCourseId(long id);

}
