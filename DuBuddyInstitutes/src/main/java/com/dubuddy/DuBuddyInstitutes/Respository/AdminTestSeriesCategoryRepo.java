package com.dubuddy.DuBuddyInstitutes.Respository;


import com.dubuddy.DuBuddyInstitutes.Entity.AdminTestSeriesCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminTestSeriesCategoryRepo extends PagingAndSortingRepository<AdminTestSeriesCategory,Long> {


    Page<AdminTestSeriesCategory> findAllByNameContaining(String name, Pageable page);

    @Query("SELECT tsc  from AdminTestSeriesCategory tsc,AdminTestSubCategories scs where tsc.id=scs.categoryId and  scs.name LIKE %:word%")
        Page<AdminTestSeriesCategory> searchTestCategoryData(String word,Pageable page);




}
