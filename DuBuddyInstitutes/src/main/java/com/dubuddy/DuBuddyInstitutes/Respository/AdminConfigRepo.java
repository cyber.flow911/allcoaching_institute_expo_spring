package com.dubuddy.DuBuddyInstitutes.Respository;

import com.dubuddy.DuBuddyInstitutes.Entity.AdminConfig;
import org.springframework.data.repository.CrudRepository;

public interface AdminConfigRepo extends CrudRepository<AdminConfig,Long> {


}
