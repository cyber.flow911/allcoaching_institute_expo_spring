package com.dubuddy.DuBuddyInstitutes.Respository;

import com.dubuddy.DuBuddyInstitutes.Entity.ContactUs;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ContactUsRepo  extends PagingAndSortingRepository<ContactUs,Long> {
}
