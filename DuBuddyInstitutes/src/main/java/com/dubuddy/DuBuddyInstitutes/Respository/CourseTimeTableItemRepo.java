package com.dubuddy.DuBuddyInstitutes.Respository;

import com.dubuddy.DuBuddyInstitutes.Entity.CourseTimeTableItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface CourseTimeTableItemRepo extends CrudRepository<CourseTimeTableItem,Long> {
    List<CourseTimeTableItem> findByInsIdAndDateTimeGreaterThanOrderByDateTimeAsc(long insId, Timestamp dateTime);

}
