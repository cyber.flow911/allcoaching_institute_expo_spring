package com.dubuddy.DuBuddyInstitutes.Respository;

import com.dubuddy.DuBuddyInstitutes.Entity.Category;
import com.dubuddy.DuBuddyInstitutes.dto.CategoryDropDownDto;
import com.dubuddy.DuBuddyInstitutes.dto.Graph2dDataDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepo  extends PagingAndSortingRepository<Category,Long> {

    @Query("SELECT new com.dubuddy.DuBuddyInstitutes.dto.CategoryDropDownDto(c.id,c.name)  from Category c")
    Iterable<CategoryDropDownDto> findByAllForDropdown();

    @Override
    Page<Category> findAll(Pageable p);

    @Query(name = "studentCountCategoryWise", nativeQuery = true)
    Iterable<Graph2dDataDto>  studentCountCategoryWise ();
    @Query(name = "InsCountCategoryWise", nativeQuery = true)
    Iterable<Graph2dDataDto>  insCountCategoryWise ();

}
