package com.dubuddy.DuBuddyInstitutes.Controller;

import com.dubuddy.DuBuddyInstitutes.Entity.Student;
import com.dubuddy.DuBuddyInstitutes.Entity.StudentPinList;
import com.dubuddy.DuBuddyInstitutes.Service.StudentPinListService;
import com.dubuddy.DuBuddyInstitutes.dto.CheckPinInputDto;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/student/pin")
@Api()
public class StudentPinListController {

    @Autowired
    private StudentPinListService studentPinListService;

    @CrossOrigin(origins = "*")
    @PostMapping("/")
    public ResponseEntity<Object> pinInstitute(@RequestBody StudentPinList studentPinList)
    {
        StudentPinList studentPinList_saved = studentPinListService.save(studentPinList);
        URI location = ServletUriComponentsBuilder.fromPath("{id}").buildAndExpand(studentPinList_saved.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/student/{page}/{pageSize}")
    public Iterable<StudentPinList> pinInstitute(@RequestBody Student student,@PathVariable int page,@PathVariable int pageSize)
    {
        return  studentPinListService.findAllByStudentId(student,page,pageSize);
    }


    @CrossOrigin(origins = "*")
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable long id)
    {
        studentPinListService.deleteById(id);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/check")
    public Optional<StudentPinList> checkForPin(@RequestBody CheckPinInputDto checkPinInputDto)
    {
       return studentPinListService.checkPin(checkPinInputDto.getStudent(),checkPinInputDto.getInstitute());

    }



}
