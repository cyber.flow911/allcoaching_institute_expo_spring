package com.dubuddy.DuBuddyInstitutes.Controller;

import com.dubuddy.DuBuddyInstitutes.Entity.Institute;
import com.dubuddy.DuBuddyInstitutes.Entity.Student;
import com.dubuddy.DuBuddyInstitutes.Service.SearchService;
import com.dubuddy.DuBuddyInstitutes.dto.AdminTestCategoriesDto;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/search")
@Api(value = "search",description = "Search controller for Student")
public class SearchController {

    @Autowired
    private SearchService searchService;

    @CrossOrigin(origins = "*")
    @GetMapping("/ins/{searchword}/{offset}/{dataLimit}")
    public   Iterable<Institute> searchInsitute(@PathVariable String searchword, @PathVariable int offset,@PathVariable int dataLimit)
    {
        return searchService.searchInstitute(searchword,offset,dataLimit);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/ins/searchbyemail/{searchword}/{offset}/{dataLimit}")
    public   Iterable<Institute> searchInstituteByEmail(@PathVariable String searchword, @PathVariable int offset,@PathVariable int dataLimit)
    {
        return searchService.searchInstituteByEmail(searchword,offset,dataLimit);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/student/searchbyemail/{searchword}/{offset}/{dataLimit}")
    public   Iterable<Student> searchStudentByEmail(@PathVariable String searchword, @PathVariable int offset,@PathVariable int dataLimit)
    {
        return searchService.searchStudentByEmail(searchword,offset,dataLimit);
    }
    @CrossOrigin(origins = "*")
    @GetMapping("/student/{searchword}/{offset}/{dataLimit}")
    public   Iterable<Student> searchStudent(@PathVariable String searchword, @PathVariable int offset, @PathVariable int dataLimit)
    {
        return searchService.searchStudent(searchword,offset,dataLimit);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/testSeries/{searchword}/{offset}/{dataLimit}")
    public   Iterable<AdminTestCategoriesDto> searchTestSeries(@PathVariable String searchword, @PathVariable int offset, @PathVariable int dataLimit)
    {
        return searchService.searchTestSeries(searchword,offset,dataLimit);
    }

}
