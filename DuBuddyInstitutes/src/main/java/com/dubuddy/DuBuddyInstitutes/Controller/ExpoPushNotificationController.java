package com.dubuddy.DuBuddyInstitutes.Controller;

import com.dubuddy.DuBuddyInstitutes.Service.InstituteService;
import com.dubuddy.DuBuddyInstitutes.Service.StudentService;
import com.dubuddy.DuBuddyInstitutes.dto.SavePushTokenDto;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/notification/")
@Api()
public class ExpoPushNotificationController {

    @Autowired
    private StudentService studentService;
    @Autowired
    private InstituteService instituteService;

    @CrossOrigin(origins="*")
    @PutMapping("/saveToken")
    public ResponseEntity<Object> savePushToken(@RequestBody SavePushTokenDto savePushTokenDto)
    {
        switch (savePushTokenDto.getMode())
        {
            case"student":
                    studentService.updatePushToken(savePushTokenDto.getId(),savePushTokenDto.getExpoToken());
                break;
            case "institute":

                    instituteService.updatePushToken(savePushTokenDto.getId(),savePushTokenDto.getExpoToken());
                break;
        }

        return ResponseEntity.ok().build();
    }
}
