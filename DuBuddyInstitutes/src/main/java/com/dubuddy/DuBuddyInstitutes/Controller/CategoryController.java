package com.dubuddy.DuBuddyInstitutes.Controller;

import com.dubuddy.DuBuddyInstitutes.Entity.Category;
import com.dubuddy.DuBuddyInstitutes.Service.CategoryService;
import com.dubuddy.DuBuddyInstitutes.dto.CategoryDropDownDto;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/category")
@Api()
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @CrossOrigin(origins = "*")
    @GetMapping("/")
    public Iterable<Category>  findAll()
    {
        return categoryService.findAll();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/")
    public ResponseEntity<Object> save(@RequestBody Category category)
    {
        Category saved_category = categoryService.save(category);
        URI location  = ServletUriComponentsBuilder.fromPath("{id}").buildAndExpand(saved_category.getId()).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Expose-Headers", "Location");
        return ResponseEntity.created(location).headers(headers).build();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    public Optional<Category> findById(@PathVariable long id)
    {
        return categoryService.findById(id);
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/dropDownMode/")
    public  Iterable<CategoryDropDownDto>  findAllForDropDown()
    {

        return categoryService.findByAllForDropdown();
    }


    @CrossOrigin(origins = "*")
    @PutMapping("/")
    public ResponseEntity<Object> edit(@RequestBody Category category)
    {
        Category saved_category = categoryService.save(category);
        URI location  = ServletUriComponentsBuilder.fromCurrentRequest().path("{id}").buildAndExpand(saved_category.getId()).toUri();
        return ResponseEntity.created(location).build();
    }


    @CrossOrigin(origins = "*")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable long id)
    {
        categoryService.delete(id);
        return ResponseEntity.ok().build();
    }
}
