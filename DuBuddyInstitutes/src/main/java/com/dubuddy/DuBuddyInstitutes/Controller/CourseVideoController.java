package com.dubuddy.DuBuddyInstitutes.Controller;

import com.dubuddy.DuBuddyInstitutes.Entity.CourseVideo;
import com.dubuddy.DuBuddyInstitutes.Entity.CourseVideoComments;
import com.dubuddy.DuBuddyInstitutes.Entity.VideoPlaylist;
import com.dubuddy.DuBuddyInstitutes.Service.CourseVideoCommentsService;
import com.dubuddy.DuBuddyInstitutes.Service.CourseVideoService;
import com.dubuddy.DuBuddyInstitutes.Service.FileUploadService;
import com.dubuddy.DuBuddyInstitutes.Utils.YTUrlExtractor;
import com.dubuddy.DuBuddyInstitutes.dto.LiveVideoDto;
import com.dubuddy.DuBuddyInstitutes.dto.YTExtractorDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/institute/course/video")
@Api()
public class CourseVideoController {

    @Autowired
    private FileUploadService fileUploadService;
    @Autowired
    private CourseVideoService courseVideoService;

    @Autowired
    private CourseVideoCommentsService courseVideoCommentsService;




    @CrossOrigin(origins = "*")
    @PostMapping("/testYt")
    public @ResponseBody YTExtractorDto test(@RequestBody String ytUrl)
    {
        return YTUrlExtractor.getYTUrlExtractor(ytUrl);

    }


    @CrossOrigin(origins = "*")
    @PostMapping("/")
    public ResponseEntity<Object> saveVideo(@RequestParam("video")String video,
                                            @RequestParam("thumb") String thumb,
                                            @RequestParam("name") String name,
                                            @RequestParam("description") String descriptions,
                                            @RequestParam("isDemo") boolean isDemo,
                                            @RequestParam("demoLength") String demoLength,
                                            @RequestParam("courseId") long courseId,
                                            @RequestParam("playlistId") long playlistId)

    {

        CourseVideo courseVideo =courseVideoService.saveCourseVideo( new CourseVideo(video,name,descriptions,isDemo,demoLength,courseId,playlistId, thumb));
        URI location = ServletUriComponentsBuilder.fromPath("{id}").buildAndExpand(courseVideo.getId()).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Expose-Headers", "Location");
        return ResponseEntity.created(location).headers(headers).build();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/saveLiveVideo")
    public ResponseEntity<Object> saveLiveVideo(@ModelAttribute LiveVideoDto liveVideoDto)

    {

        try {
            String videoThumb = liveVideoDto.getThumbnail();
            System.out.println(videoThumb+" videoThumb");
            YTExtractorDto ytExtractorDto = new YTExtractorDto();
            if(!liveVideoDto.getVideoLocation().isEmpty())
            {
                ytExtractorDto =  YTUrlExtractor.getYTUrlExtractor(liveVideoDto.getVideoLocation());
            }
            CourseVideo courseVideo = new CourseVideo(ytExtractorDto.getUrl(),liveVideoDto.getName(),liveVideoDto.getDescription(),liveVideoDto.isDemo(),null,liveVideoDto.getCourseId(),0,videoThumb);
            courseVideo.setVideoType("live");
            courseVideo.setLiveClassDate(liveVideoDto.getLiveClassDate());
            courseVideo.setLiveClassTime(liveVideoDto.getLiveClassTime());
            courseVideo.setId(liveVideoDto.getId());
            courseVideo.setVideoFormatJson(new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(ytExtractorDto));
            CourseVideo courseVideo_saved =courseVideoService.saveCourseVideo(courseVideo);
            URI location = ServletUriComponentsBuilder.fromPath("{id}").buildAndExpand(courseVideo_saved.getId()).toUri();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Access-Control-Expose-Headers", "Location");
            return ResponseEntity.created(location).headers(headers).build();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }



    }

    @CrossOrigin(origins = "*")
    @PutMapping("/updateVideo")
    public ResponseEntity<Object> updateVideo(@RequestParam("video") String file,@RequestParam("videoId") long videoId)
    {

        courseVideoService.updateVideoLink(videoId, file);
        URI location = ServletUriComponentsBuilder.fromPath("{location}").buildAndExpand(file).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Expose-Headers", "Location");
        return ResponseEntity.created(location).headers(headers).build();
    }

    @CrossOrigin(origins = "*")
    @PutMapping("/updateLiveVideoLink/{videoId}")
    public ResponseEntity<Object> updateLiveVideoLink(@RequestBody String videoLink,@PathVariable("videoId") long videoId)
    {


        try {
            YTExtractorDto ytExtractorDto = YTUrlExtractor.getYTUrlExtractor(videoLink);
            courseVideoService.updateLiveVideoLink(videoId,ytExtractorDto.getUrl(),new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(ytExtractorDto));
            URI location = ServletUriComponentsBuilder.fromPath("{location}").buildAndExpand(videoLink).toUri();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Access-Control-Expose-Headers", "Location");
            return ResponseEntity.created(location).headers(headers).build();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }

    }

    @CrossOrigin(origins = "*")
    @PutMapping("/updateVideoThumb")
    public ResponseEntity<Object> updateVideoThumb(@RequestParam("thumbnail") String thumbnail,@RequestParam("videoId") long videoId)
    {
        courseVideoService.updateVideoThumbLink(videoId, thumbnail);
        URI location = ServletUriComponentsBuilder.fromPath("{location}").buildAndExpand(thumbnail).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Expose-Headers", "Location");
        return ResponseEntity.created(location).headers(headers).build();
    }
    @CrossOrigin(origins = "*")
    @PutMapping("/editVideoDetails")
    public ResponseEntity<Object> editDetails(@RequestBody CourseVideo courseVideo)
    {
        courseVideoService.saveCourseVideo(courseVideo);
        return ResponseEntity.ok().build();
    }

//    @GetMapping("/playlist/{id}")
//    public Iterable<CourseVideo> findByPlalist(long id)
//    {
//        return courseVideoService.findByPlaylist(id);
//
//    }

    @CrossOrigin(origins = "*")
    @GetMapping("/all/{id}/{offset}/{dataLimit}")
    public Page<CourseVideo> findByCourse(@PathVariable  long id,@PathVariable int offset,@PathVariable int dataLimit)
    {
        return courseVideoService.findByCourseId(id,offset,dataLimit);

    }

    @CrossOrigin(origins = "*")
    @GetMapping("/liveVideosOfCourse/{id}/{offset}/{dataLimit}")
    public Iterable<CourseVideo> findLiveVideoByCourse(@PathVariable  long id,@PathVariable int offset,@PathVariable int dataLimit)
    {
        return courseVideoService.findLiveVideosByCourseId(id,offset,dataLimit);

    }


    //fetching playlist by course id and hidden parameter
    @CrossOrigin(origins = "*")
    @GetMapping("/all/{id}/hidden/{hidden}/{offset}/{dataLimit}")
    public Page<CourseVideo> findByCourse(@PathVariable  long id, @PathVariable boolean hidden, @PathVariable int offset, @PathVariable int dataLimit)
    {
        return courseVideoService.findByCourseIdAndHidden(id,hidden,offset,dataLimit);

    }
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    public Optional<CourseVideo> findById(@PathVariable long id)
    {
        return courseVideoService.findById(id);

    }
    @CrossOrigin(origins = "*")
    @PostMapping("/createPlaylist")
    public ResponseEntity<Object> createPlaylist(@RequestBody VideoPlaylist videoPlaylist)
    {
        VideoPlaylist videoPlaylist_saved = courseVideoService.createPlaylist(videoPlaylist);
        URI location = ServletUriComponentsBuilder.fromPath("{id}").buildAndExpand(videoPlaylist_saved.getId()).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Expose-Headers", "Location");

        return ResponseEntity.created(location).headers(headers).build();

    }

    //mapping for fetching video playlists
    @CrossOrigin(origins = "*")
    @GetMapping("/playlists/{id}")
    public Iterable<VideoPlaylist> findVideoPlaylist(@PathVariable long id)
    {
        return courseVideoService.findPlaylists(id);
    }

    //mapping for fetching videos of a playlist
    @CrossOrigin(origins = "*")
    @GetMapping("/playlist/{id}/{offset}/{dataLimit}")
    public  Page<CourseVideo> findPlaylistById(@PathVariable long id,@PathVariable int offset,@PathVariable int dataLimit)
    {
        return courseVideoService.findByPlaylist(id,offset,dataLimit);
    }

    //mapping for fetching videos of a playlist with hidden parameter
    @CrossOrigin(origins = "*")
    @GetMapping("/playlist/{id}/hidden/{hidden}/{offset}/{dataLimit}")
    public  Page<CourseVideo> findPlaylistByIdAndHidden(@PathVariable long id,@PathVariable boolean hidden,@PathVariable int offset,@PathVariable int dataLimit)
    {
        return courseVideoService.findByPlaylistAndHidden(id,hidden,offset,dataLimit);
    }


    @CrossOrigin(origins="*")
    @PutMapping("/publish/{status}/{id}")
    public  ResponseEntity<Object> updatePublishedStatus(@PathVariable boolean status,@PathVariable long id)
    {
        courseVideoService.updatePublishedStatusById(status,id);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins="*")
    @PutMapping("/hidden/{status}/{id}")
    public  ResponseEntity<Object> updateHiddenStatus(@PathVariable boolean status,@PathVariable long id)
    {
        courseVideoService.updateHiddenStatusById(status,id);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins="*")
    @PutMapping("/demo/{status}/{id}")
    public  ResponseEntity<Object> updateDemoStatus(@PathVariable boolean status,@PathVariable long id)
    {
        courseVideoService.updateDemoStatusById(status,id);
        return ResponseEntity.ok().build();
    }
    @CrossOrigin(origins="*")
    @PutMapping("/streaming/{status}/{id}")
    public  ResponseEntity<Object> updateStreamingStatus(@PathVariable boolean status,@PathVariable long id)
    {
        courseVideoService.updateStreamingStatus(status,id);
        return ResponseEntity.ok().build();
    }
    @CrossOrigin(origins="*")
    @PutMapping("/updatePlaylist/{playlist_id}/{id}")
    public  ResponseEntity<Object> updateHiddenStatus(@PathVariable long playlist_id,@PathVariable long id)
    {
        courseVideoService.updatePlaylistIdById(playlist_id,id);
        return ResponseEntity.ok().build();
    }
    @CrossOrigin(origins = "*")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> delete(@PathVariable long id)
    {
        courseVideoService.delete(id);
        return ResponseEntity.ok().build();
    }


    //count course videos
    @CrossOrigin(origins = "*")
    @GetMapping("/count/{courseId}/{videoType}")
    public long countVideoByCourse(@PathVariable  long courseId,@PathVariable String videoType )
    {
        return courseVideoService.countByCourseIdAndVideoType(courseId,videoType);
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/comment/add")
    public ResponseEntity<Object> addComment(@RequestBody CourseVideoComments courseVideoComments)
    {
        CourseVideoComments courseVideoComments_saved = courseVideoCommentsService.addComment(courseVideoComments);
        URI location  = ServletUriComponentsBuilder.fromPath("{id}").buildAndExpand(courseVideoComments_saved.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/views/{v_id}")
    public ResponseEntity<Object> videoView(@PathVariable long v_id )
    {
         courseVideoService.updateVideoViewsById(v_id);
        return ResponseEntity.ok().build();
    }

    @CrossOrigin(origins = "*")
    @GetMapping("/comment/{v_id}/{page}/{page_size}")
    public Iterable<CourseVideoComments> fetch_comments(@PathVariable long v_id,@PathVariable int page,@PathVariable int page_size)
    {
        return  courseVideoCommentsService.fetch_comments(v_id,page,page_size);
    }


    //delete Video playlist
    @CrossOrigin(origins = "*")
    @DeleteMapping("/delete/playlist/{playlistId}")
    public ResponseEntity<Object> deletePlaylist(@PathVariable  long playlistId)
    {
        courseVideoService.deletePlaylist(playlistId);
        return  ResponseEntity.ok().build();
    }
}
