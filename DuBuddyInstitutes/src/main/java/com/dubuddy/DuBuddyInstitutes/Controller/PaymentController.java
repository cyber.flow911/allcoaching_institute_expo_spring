package com.dubuddy.DuBuddyInstitutes.Controller;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import com.dubuddy.DuBuddyInstitutes.Entity.Course;
import com.dubuddy.DuBuddyInstitutes.Entity.InsReview;
import com.dubuddy.DuBuddyInstitutes.Entity.Transaction;
import com.dubuddy.DuBuddyInstitutes.Service.CourseService;
import com.dubuddy.DuBuddyInstitutes.Service.InsReviewService;
import com.dubuddy.DuBuddyInstitutes.Service.TransactionService;
import com.dubuddy.DuBuddyInstitutes.Utils.CashFree.CashFreeOrderCreation;
import com.dubuddy.DuBuddyInstitutes.Utils.PaytmGateway.PaytmDetailPojo;
import com.dubuddy.DuBuddyInstitutes.Utils.RandomString;
import com.dubuddy.DuBuddyInstitutes.dto.CashFree.Order.CashFreeOrderDto;
import com.dubuddy.DuBuddyInstitutes.Service.PayoutService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import com.paytm.pg.merchant.PaytmChecksum;

import static java.lang.Float.parseFloat;

@RestController
@RequestMapping("/api/v1/")
@Api()
public class PaymentController {

    @Autowired
    private PaytmDetailPojo paytmDetailPojo;
    @Autowired
    private Environment env;
    @Autowired
    private CourseService courseService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    private InsReviewService insReviewService;
    @Autowired
    private PayoutService payoutService;


    @PostMapping("/checkout/course/")
    public Optional<CashFreeOrderDto> checkout(@RequestBody Transaction transaction) throws Exception {
        Optional<Course> course = courseService.findById(transaction.getCourseId());
        System.out.println(transaction);
        System.out.println(course);
        if (course.isPresent()) {
            Course courseObj = course.get();
            float fees = courseObj.getFees();
            long transactionCount = transactionService.countTransactions();
            String orderId = transactionCount + "" + RandomString.getAlphaNumericString(6);
            transaction.setAmount(String.valueOf(fees));
            transaction.setOrderId(orderId);
            transaction.setStatus(Transaction.TXN_STATUS.PENDING);
            transaction.setProductType(Transaction.ProductType.COURSE);
            String notifyUrl = null;
            String orderNote = "For Enrolling in DuBuddy Course";
            CashFreeOrderDto cashFreeOrderDto = CashFreeOrderCreation.createOder("DEBUG", fees, orderId, transaction.getStudentId(), transaction.getName(), transaction.getEmail(), transaction.getMobile(), notifyUrl, orderNote);
            transaction.setGatewayTransactionId(String.valueOf(cashFreeOrderDto.getCf_order_id()));
            transactionService.addTransaction(transaction);
            return Optional.of(cashFreeOrderDto);
        }
        return Optional.empty();
    }

    @PostMapping("/checkout/course/updateTransaction/{orderId}")
    public ResponseEntity<Object> checkout(@PathVariable String orderId) throws Exception {
        Optional<Transaction> transaction = transactionService.findByOrderId(orderId);
        try {

            if (transaction.isPresent()) {
                Transaction transactionObj = transaction.get();
                CashFreeOrderDto cashFreeOrderDto = CashFreeOrderCreation.ValidateOrder("DEBUG", transactionObj.getOrderId());
                System.out.println(cashFreeOrderDto);
//                if (cashFreeOrderDto.getOrder_status().equalsIgnoreCase("PAID")) {
                    if(processPayment(transactionObj))
                    {
                        transactionObj.setStatus(Transaction.TXN_STATUS.SUCCESS);
                        transactionService.addTransaction(transactionObj);
                        return ResponseEntity.ok().build();
                    }else{
                        issueRefundToUser(transactionObj,"Transaction Successfull, but some thing went wrong while processing transaction");
                        return ResponseEntity.internalServerError().build();
                    }

//                } else {
//
//                    issueRefundToUser(transactionObj,"Payment is Not Done By User or got Failed at Cash Free, as status at cash free is not paid for this transaction");
//                    return ResponseEntity.badRequest().build();
//                }
            }else{
                transaction.ifPresent(value->{
                    issueRefundToUser(value,"transaction not found corresponding to provided orderId :"+orderId);
                });
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {

            transaction.ifPresent(value->{
                issueRefundToUser(value,"Something went Wrong while updating and processing transaction");
            });

            return ResponseEntity.internalServerError().build();
        }
    }

void issueRefundToUser(Transaction transaction,String reason){
    CashFreeOrderCreation.RefundOrder("DEBUG", transaction.getOrderId(), parseFloat(transaction.getAmount()));
    transaction.setStatus(Transaction.TXN_STATUS.FAILED);
    transaction.setRefund_status(Transaction.Refund_Status.DONE_BY_DUBUDDY);
    if(!reason.isEmpty()){
            transaction.setReason_of_failure(reason);
    }
    transactionService.addTransaction(transaction);
}
    //    @PostMapping(value = "/submitPaymentDetail")
//    public ModelAndView getRedirect(@RequestParam(name = "CUST_ID") String customerId,
//                                    @RequestParam(name = "TXN_AMOUNT") String transactionAmount,
//                                    @RequestParam(name = "ORDER_ID") String orderId) throws Exception {
//            return redirectToPaytm(customerId,transactionAmount,orderId);
//    }
//
//    private ModelAndView redirectToPaytm(String customerId,String transactionAmount,String orderId) throws  Exception
//    {
//
//        ModelAndView modelAndView = new ModelAndView("redirect:" + paytmDetailPojo.getPaytmUrl());
//        TreeMap<String, String> parameters = new TreeMap<>();
//        paytmDetailPojo.getDetails().forEach((k, v) -> parameters.put(k, v));
//        parameters.put("MOBILE_NO", env.getProperty("paytm.mobile"));
//        parameters.put("EMAIL", env.getProperty("paytm.email"));
//        parameters.put("ORDER_ID", orderId);
//        parameters.put("TXN_AMOUNT", transactionAmount);
//        parameters.put("CUST_ID", customerId);
//        String checkSum = getCheckSum(parameters);
//        parameters.put("CHECKSUMHASH", checkSum);
//        modelAndView.addAllObjects(parameters);
//        return modelAndView;
//    }
    @PostMapping(value = "/pgresponse")
    public String getResponseRedirect(HttpServletRequest request, Model model) {

        Map<String, String[]> mapData = request.getParameterMap();
        TreeMap<String, String> parameters = new TreeMap<String, String>();
        String paytmChecksum = "";
        for (Entry<String, String[]> requestParamsEntry : mapData.entrySet()) {
            if ("CHECKSUMHASH".equalsIgnoreCase(requestParamsEntry.getKey())) {
                paytmChecksum = requestParamsEntry.getValue()[0];
            } else {
                parameters.put(requestParamsEntry.getKey(), requestParamsEntry.getValue()[0]);
            }
        }
        String result;
        String txnStatus = parameters.get("STATUS");
        String txnId = parameters.get("TXNID");
        String orderId = parameters.get("ORDERID");
        String gatewayResponseMsg = parameters.get("RESPMSG");
        boolean isValideChecksum = false;
        System.out.println(parameters);

        transactionService.compeleteTransaction(txnStatus, txnId, gatewayResponseMsg, orderId);
        Optional<Transaction> transaction = transactionService.findByOrderId(orderId);
        try {
            isValideChecksum = validateCheckSum(parameters, paytmChecksum);
            if (isValideChecksum && parameters.containsKey("RESPCODE")) {
                if (parameters.get("RESPCODE").equals("01")) {
                    result = "Payment Successful";


                    transaction.ifPresent(this::processPayment);


                } else {
                    result = "Payment Failed";
                }
            } else {
                result = "Checksum mismatched";
            }
        } catch (Exception e) {
            result = e.toString();
        }

        System.out.println(result);
        model.addAttribute("result", result);
        parameters.remove("CHECKSUMHASH");
        model.addAttribute("parameters", parameters);
        model.addAttribute("insId", transaction.get().getInsId());
        return "pgResponse";
    }

    private boolean processPayment(Transaction transactionData) {
        try {
            switch (transactionData.getProductType()) {
                case COURSE:
                    switch (transactionData.getPurchaseMode()) {
                        case SINGLE:
                            InsReview insReview = new InsReview(transactionData.getCourseId(), transactionData.getInsId(), transactionData.getStudentId(),transactionData.getStudentImage(),transactionData.getName());
                            insReviewService.save(insReview);
                            return true;
                        case MULTIPLE:
                            return true;
                        case ALL:
                            ArrayList<InsReview> insReviewsArr = new ArrayList<>();
                            boolean shouldDeletedCoursesBeConsidered = false;
                            Iterable<Course> insActiveCourses = courseService.findByInstId(transactionData.getInsId(), shouldDeletedCoursesBeConsidered);
                            insActiveCourses.forEach(course -> {
                                insReviewsArr.add(new InsReview(course.getId(), transactionData.getInsId(), transactionData.getStudentId(),transactionData.getStudentImage(),transactionData.getName()));
                            });
                            insReviewService.saveBulk(insReviewsArr);
                            return true;
                    }
                    return true;

                //more cases to be added here in future if any other kind of product is introduced
                default:
                    System.out.println("not product found related to : " + transactionData.getProductType());
                    return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    private boolean validateCheckSum(TreeMap<String, String> parameters, String paytmChecksum) throws Exception {
        return PaytmChecksum.verifySignature(parameters,
                paytmDetailPojo.getMerchantKey(), paytmChecksum);
    }

    private String getCheckSum(TreeMap<String, String> parameters) throws Exception {
        return PaytmChecksum.generateSignature(parameters, paytmDetailPojo.getMerchantKey());
    }

}
