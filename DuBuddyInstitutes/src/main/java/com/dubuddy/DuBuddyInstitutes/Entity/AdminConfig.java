package com.dubuddy.DuBuddyInstitutes.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@NoArgsConstructor
@ToString
@Data
public class AdminConfig {

    @Id
    @GeneratedValue
    private long id;

    private String paymentCommission;
    private String globalCourseDiscountAmt;

}
