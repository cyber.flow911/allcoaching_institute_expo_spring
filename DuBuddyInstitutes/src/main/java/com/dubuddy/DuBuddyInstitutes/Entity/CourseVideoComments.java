package com.dubuddy.DuBuddyInstitutes.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Data
@ToString
@NoArgsConstructor
@Entity
public class CourseVideoComments {

    @Id
    @GeneratedValue
    private long id;

    private String comment;

    private long studentId;
    private  String studentName;
    private String studentImage;

    private long videoId;

    private boolean isPinnedComment;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date commentTime;



}
