package com.dubuddy.DuBuddyInstitutes.Entity;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Data
@ToString
@NoArgsConstructor
@Entity
public class CourseTimeTableSubject {

    @Id
    @GeneratedValue
    private long id;

    private String name;
    private long courseId;

    public CourseTimeTableSubject(String name, long courseId) {
        this.name = name;
        this.courseId = courseId;
    }
}
