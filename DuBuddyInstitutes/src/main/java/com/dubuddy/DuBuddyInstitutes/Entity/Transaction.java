package com.dubuddy.DuBuddyInstitutes.Entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;




@Data
@ToString
@NoArgsConstructor
@Entity
public class Transaction {

    public   enum PurchaseMode {
        SINGLE,MULTIPLE,ALL
    }

    public   enum ProductType {
        COURSE,OTHER
    }

    public enum TXN_STATUS {
        PENDING,SUCCESS,FAILED
    }

    public enum Refund_Status{
        CANCELLED,PENDING,SUCCESS,ONHOLD,DONE_BY_DUBUDDY
    }
    @Id
    @GeneratedValue
    private  long id;

    private long insId;
    private long studentId;
    private String studentImage;
    private long courseId;

    private String name;
    private String email;
    private String mobile;

    @Enumerated(EnumType.ORDINAL)
    private PurchaseMode purchaseMode; //will be used to identify single course,multiple course or All courses of institute
    private String amount;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date purchaseDate;
    private String orderId;

    private String gatewayTransactionId;
    private String gatewayResponseMsg;

    @Enumerated(EnumType.ORDINAL)
    private TXN_STATUS status;
    @Enumerated(EnumType.ORDINAL)
    private ProductType productType;//COURSE or Any other digital product of dubuddy
    private boolean isSeenByAdmin;
    private boolean isSeenByIns;
    @Enumerated(EnumType.ORDINAL)
    private Refund_Status refund_status;
    private String reason_of_failure;
    public Transaction(long insId, long studentId, long courseId, String amount, String orderId, TXN_STATUS status,ProductType productType,String studentImage) {
        this.insId = insId;
        this.studentId = studentId;
        this.courseId = courseId;
        this.amount = amount;
        this.orderId = orderId;
        this.status = status;
        this.productType = productType;
        this.studentImage = studentImage;

    }
}
