package com.dubuddy.DuBuddyInstitutes.Service;

import com.dubuddy.DuBuddyInstitutes.Entity.Category;
import com.dubuddy.DuBuddyInstitutes.Respository.CategoryRepo;
import com.dubuddy.DuBuddyInstitutes.dto.CategoryDropDownDto;
import com.dubuddy.DuBuddyInstitutes.dto.Graph2dDataDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepo categoryRepo;




    //saving category to repo
    public Category save(Category category)
    {
        return categoryRepo.save(category);
    }

    public void delete(long id)
    {
          categoryRepo.deleteById(id);
    }

    //fetching all categories from repo
    public Iterable<Category> findAll()
    {

        Page<Category> pagedResult =categoryRepo.findAll(PageRequest.of(0,200, Sort.by("sortOrder")));
        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Category>();
        }

    }

    //fetching single category from repo using id
    public Optional<Category> findById(long id)
    {
        return categoryRepo.findById(id);
    }

    //fetching all for dropdown mode as label and value pair
    public  Iterable<CategoryDropDownDto> findByAllForDropdown()
    {
        return categoryRepo.findByAllForDropdown();
    }

    //student count category wise
    public  Iterable<Graph2dDataDto> studentCountCategoryWise()
    {
        return categoryRepo.studentCountCategoryWise();
    }

    //ins count category wise
    public  Iterable<Graph2dDataDto> insCountCategoryWise()
    {
        return categoryRepo.insCountCategoryWise();
    }




}
