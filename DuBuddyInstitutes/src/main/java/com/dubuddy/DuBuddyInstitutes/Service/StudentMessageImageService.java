package com.dubuddy.DuBuddyInstitutes.Service;

import com.dubuddy.DuBuddyInstitutes.Entity.StudentMessageImages;
import com.dubuddy.DuBuddyInstitutes.Respository.StudentMessageImagesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class StudentMessageImageService {


    @Autowired
    private StudentMessageImagesRepo studentMessageImagesRepo;


    public void addStudentMessageImagesList (ArrayList<StudentMessageImages> studentMessageImages)
    {
        Iterable<StudentMessageImages>  studentMessageImages_saved = studentMessageImagesRepo.saveAll(studentMessageImages);


    }
}
