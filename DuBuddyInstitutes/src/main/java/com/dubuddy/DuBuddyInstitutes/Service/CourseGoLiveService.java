package com.dubuddy.DuBuddyInstitutes.Service;

import com.dubuddy.DuBuddyInstitutes.Entity.CourseGoLive;
import com.dubuddy.DuBuddyInstitutes.Respository.CourseGoLiveRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CourseGoLiveService {

    @Autowired
    private CourseGoLiveRepo courseGoLiveRepo;

    //inserting courseGOLIVE TO REPO
    public CourseGoLive save(CourseGoLive courseGoLive)
    {
        return courseGoLiveRepo.save(courseGoLive);
    }

    //find all golive using courseId
    public Iterable<CourseGoLive> findByCourseId(long id)
    {
        return courseGoLiveRepo.findByCourseId(id);
    }
    //find single golive by id
    public Optional<CourseGoLive> findById(long id)
    {
        return courseGoLiveRepo.findById(id);
    }
}


