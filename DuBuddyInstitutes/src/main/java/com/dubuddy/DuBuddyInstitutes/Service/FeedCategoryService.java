package com.dubuddy.DuBuddyInstitutes.Service;

import com.dubuddy.DuBuddyInstitutes.Entity.FeedCategory;
import com.dubuddy.DuBuddyInstitutes.Respository.FeedCategoryRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FeedCategoryService {

    @Autowired
    private FeedCategoryRepo feedCategoryRepo;

    //fetch feed categories

    public Iterable<FeedCategory> findAll()
    {
        return    feedCategoryRepo.findAll();
    }

    //saving feed Category
    public FeedCategory save(FeedCategory feedCategory)
    {
        return feedCategoryRepo.save(feedCategory);
    }

    //find by category id
    public Optional<FeedCategory> findById(long id)
    {
        return feedCategoryRepo.findById(id);
    }
}
