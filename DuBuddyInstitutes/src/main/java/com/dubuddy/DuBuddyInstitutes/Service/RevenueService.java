package com.dubuddy.DuBuddyInstitutes.Service;

import com.dubuddy.DuBuddyInstitutes.Entity.Course;
import com.dubuddy.DuBuddyInstitutes.dto.InsLeadsStudentDto;
import com.dubuddy.DuBuddyInstitutes.dto.SalesOverViewDataDto;
import com.dubuddy.DuBuddyInstitutes.dto.SalesWithRevenueOverView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RevenueService {

  @Autowired
  private InsReviewService insReviewService;

  @Autowired
  private CourseService courseService;

  public SalesWithRevenueOverView getSalesOverview(long insId)
  {
      Iterable<SalesOverViewDataDto> salesOverViewDataDtos = insReviewService.salesOverViewDataData(insId);
      final float[] sum = {0};
      final long[] count = {0};
       salesOverViewDataDtos.forEach(item->{
         count[0]++;
         Course c = courseService.findById(item.getCourseId()). get();
         float courseRevenue = (c.getFees()*item.getTotal());
         sum[0] = sum[0] +courseRevenue;
         item.setCourseFee(c.getFees());
         item.setTotalCourseRevenue(courseRevenue);
         item.setCourseName(c.getTitle());

       });
    SalesWithRevenueOverView salesWithRevenueOverView = new SalesWithRevenueOverView(salesOverViewDataDtos,count[0],sum[0]);
      return  salesWithRevenueOverView;


  }

  public Iterable<InsLeadsStudentDto> studentList(long courseId, int page, int pageSize)
  {
      return insReviewService.findStudentList(courseId,page,pageSize);
  }





}
