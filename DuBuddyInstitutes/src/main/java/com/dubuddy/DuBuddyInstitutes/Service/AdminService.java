package com.dubuddy.DuBuddyInstitutes.Service;

import com.dubuddy.DuBuddyInstitutes.Entity.Admin;
import com.dubuddy.DuBuddyInstitutes.Entity.AdminConfig;
import com.dubuddy.DuBuddyInstitutes.Respository.AdminConfigRepo;
import com.dubuddy.DuBuddyInstitutes.Respository.AdminRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminService {


    @Autowired
    private AdminRepo adminRepo;

    @Autowired
    private AdminConfigRepo adminConfigRepo;

    public boolean authCheck(Admin admin)
    {
            return adminRepo.findByEmailAndPassword(admin.getEmail(),admin.getPassword());
    }


    public AdminConfig  updateConfig(AdminConfig config)
    {
        return  adminConfigRepo.save(config);
    }

    public AdminConfig getConfig()
    {
        Iterable<AdminConfig> adminConfigs = adminConfigRepo.findAll();
        return adminConfigs.iterator().next();
    }
}
