package com.dubuddy.DuBuddyInstitutes.Service;

import com.dubuddy.DuBuddyInstitutes.Entity.AdminTestSubCategories;
import com.dubuddy.DuBuddyInstitutes.Respository.AdminTestSeriesSubCategoryContentRepo;
import com.dubuddy.DuBuddyInstitutes.Respository.AdminTestSubCategoriesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AdminTestSubCategoriesService {


    @Autowired
    private AdminTestSubCategoriesRepo adminTestSubCategoriesRepo;

    @Autowired
    private AdminTestSeriesSubCategoryContentRepo adminTestSeriesSubCategoryContentRepo;

    @Autowired
    private AdminTestSeriesCategoryService adminTestSeriesCategoryService;
    public AdminTestSubCategories save(AdminTestSubCategories adminTestSubCategories)
    {
        return  adminTestSubCategoriesRepo.save(adminTestSubCategories);
    }

    public Iterable<AdminTestSubCategories> findAllItems()
    {
        return adminTestSubCategoriesRepo.findAll();
    }

    public Iterable<AdminTestSubCategories> findByCategory(long id)
    {
        return adminTestSubCategoriesRepo.findByCategoryId(id);
    }



    public Iterable<AdminTestSubCategories> findByCategoryPagedResult(long id,int offset,int pageSize)
    {
        Page<AdminTestSubCategories> adminTestSubCategories = adminTestSubCategoriesRepo.findByCategoryId(id,PageRequest.of(offset,pageSize));
        if(adminTestSubCategories.hasContent())
        {
            return  adminTestSubCategories.getContent();
        }else
        {
            return  new ArrayList<>();
        }
    }

//    public  Iterable<AdminTestCategoriesDto> searchTestCategoryData(String search, int page, int pageSize)
//    {
//        Page<AdminTestSubCategories> adminTestSeriesCategories = adminTestSubCategoriesRepo.findByNameContaining(search, PageRequest.of(page,pageSize));
//        if(adminTestSeriesCategories.hasContent())
//        {
//
//            List<AdminTestCategoriesDto> adminTestCategoriesDtos = new ArrayList<>();
//            adminTestSeriesCategories.getContent().forEach(item -> {
//                Optional<AdminTestSeriesCategory> adminTestCategories = adminTestSeriesCategoryService.findById(item.getId());
//                adminTestCategoriesDtos.add(new AdminTestCategoriesDto(adminTestCategories.get().getId(), adminTestCategories.get().getName(), item));
//            });
//            return adminTestCategoriesDtos;
//        }else
//        {
//            return new ArrayList<>();
//        }
//
//    }


    public  void delete(long id)
    {
        adminTestSubCategoriesRepo.deleteById(id);

    }




}
