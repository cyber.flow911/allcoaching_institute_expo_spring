package com.dubuddy.DuBuddyInstitutes.Service;

import com.dubuddy.DuBuddyInstitutes.Entity.AdminTestSeriesSubCategoryContent;
import com.dubuddy.DuBuddyInstitutes.Respository.AdminTestSeriesSubCategoryContentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class AdminTestSeriesSubCategoryContentService {

     @Autowired
    private AdminTestSeriesSubCategoryContentRepo adminTestSeriesSubCategoryContentRepo;

     public AdminTestSeriesSubCategoryContent addSubCategoryItem(AdminTestSeriesSubCategoryContent adminTestSeriesSubCategoryContent)
     {
            return adminTestSeriesSubCategoryContentRepo.save(adminTestSeriesSubCategoryContent);
     }


     public Iterable<AdminTestSeriesSubCategoryContent> findAllContentBySubCategory(int page,int pageSize,long subCategoryId)
     {
          Page<AdminTestSeriesSubCategoryContent> pagedResult =  adminTestSeriesSubCategoryContentRepo.findAllByTestSeriesSubCategoryId(subCategoryId, PageRequest.of(page,pageSize, Sort.by(Sort.Direction.DESC,"addDate")));
          if(pagedResult.hasContent())
          {
              return pagedResult.getContent();
          }else
          {
              return new ArrayList<>();
          }

     }

     public void deleteById(long id)
     {
         adminTestSeriesSubCategoryContentRepo.deleteById(id);
     }



}
