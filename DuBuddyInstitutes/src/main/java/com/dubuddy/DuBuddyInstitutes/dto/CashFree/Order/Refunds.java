package com.dubuddy.DuBuddyInstitutes.dto.CashFree.Order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
public class Refunds {

    private String url;
}
