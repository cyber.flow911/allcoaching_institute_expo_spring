package com.dubuddy.DuBuddyInstitutes.dto;

import com.dubuddy.DuBuddyInstitutes.Entity.InsTestSeries;
import com.dubuddy.DuBuddyInstitutes.Entity.InsTestSeriesQuestions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TestSeriesDto {
    private InsTestSeries testSeries;

    private List<InsTestSeriesQuestions> questions;
}
