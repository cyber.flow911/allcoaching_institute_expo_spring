package com.dubuddy.DuBuddyInstitutes.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class StudentPurchaseDto {


    private long insId;
    private String insName;
    private String insImage;
    private String CourseName;

}
