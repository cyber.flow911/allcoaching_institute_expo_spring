package com.dubuddy.DuBuddyInstitutes.dto.CashFree.Order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
public class OrderMeta {
    private String return_url;
    private String notify_url;
    private String payment_methods;
}
