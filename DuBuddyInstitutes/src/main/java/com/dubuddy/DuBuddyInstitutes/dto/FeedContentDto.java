package com.dubuddy.DuBuddyInstitutes.dto;

import com.dubuddy.DuBuddyInstitutes.Entity.Feed;
import com.dubuddy.DuBuddyInstitutes.Entity.FeedImages;
import com.dubuddy.DuBuddyInstitutes.Entity.FeedPollOptions;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class FeedContentDto {
    private Feed feed;
    private Iterable<FeedPollOptions> feedPollOptions;
    private Iterable<FeedImages> feedImages;

    public FeedContentDto(Feed feed) {
        this.feed = feed;
    }

    public FeedContentDto(Feed feed, Iterable<FeedPollOptions> feedPollOptions) {
        this.feed = feed;
        this.feedPollOptions = feedPollOptions;
    }

}
