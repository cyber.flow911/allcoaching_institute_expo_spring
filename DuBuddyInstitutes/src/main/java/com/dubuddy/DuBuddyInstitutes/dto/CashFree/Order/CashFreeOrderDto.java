package com.dubuddy.DuBuddyInstitutes.dto.CashFree.Order;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
@Data
@NoArgsConstructor
public class CashFreeOrderDto {
    private long cf_order_id;
    private String order_id;
    private boolean isSuccess;
    private  String order_status;
    private String payment_session_id;
    private String order_token;
    private String order_note;
    private String payment_link;
    private float order_amount;
    private String order_currency;
    private String entity;
    private CustomerDetails customer_details;
    private OrderMeta order_meta;
    private Payments payments;
    private Settlements settlements;
    private Refunds refunds;
}
