package com.dubuddy.DuBuddyInstitutes.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SavePushTokenDto {

    private long id;
    private String mode;
    private String expoToken;
}
