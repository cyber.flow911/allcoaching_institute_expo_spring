package com.dubuddy.DuBuddyInstitutes.dto;


import com.dubuddy.DuBuddyInstitutes.Entity.CourseTimeTableItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CourseTimeTableDto {
    private long id;
    private String name;
    private Iterable<CourseTimeTableItem> courseTimeTableItem;
}
