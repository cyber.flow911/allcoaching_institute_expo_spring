package com.dubuddy.DuBuddyInstitutes.dto.CashFree.Order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Data
@NoArgsConstructor
public class CustomerDetails {
    private String customer_id;
    private String customer_name;
    private  String customer_email;
    private  String customer_phone;
}
