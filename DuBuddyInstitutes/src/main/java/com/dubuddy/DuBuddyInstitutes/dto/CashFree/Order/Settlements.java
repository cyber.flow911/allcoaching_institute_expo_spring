package com.dubuddy.DuBuddyInstitutes.dto.CashFree.Order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
@Data
@NoArgsConstructor
public class Settlements {

    private String url;
}
