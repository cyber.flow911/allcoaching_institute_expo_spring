package com.dubuddy.DuBuddyInstitutes.dto;

import com.dubuddy.DuBuddyInstitutes.Entity.Institute;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class InstituteRevenueDto {

    private Institute institute;
    private Double todayRevenue;
    private Double totalRevenue;
}
