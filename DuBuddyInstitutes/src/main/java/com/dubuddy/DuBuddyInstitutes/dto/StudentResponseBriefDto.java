package com.dubuddy.DuBuddyInstitutes.dto;

import com.dubuddy.DuBuddyInstitutes.Entity.InsTestSeriesUserResponseBrief;
import com.dubuddy.DuBuddyInstitutes.Entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class StudentResponseBriefDto {

    private Student student;
    private InsTestSeriesUserResponseBrief responseBrief;
}
