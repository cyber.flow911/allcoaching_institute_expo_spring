package com.dubuddy.DuBuddyInstitutes.dto;

import com.dubuddy.DuBuddyInstitutes.Entity.InsTestSeriesQuestions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TestSeriesQuestionDto {

    private InsTestSeriesQuestions question;
    private String userResponse=null;
    private String status=null;//correct , wrong or unattempted

    public TestSeriesQuestionDto(InsTestSeriesQuestions question) {
        this.question = question;
    }
}
