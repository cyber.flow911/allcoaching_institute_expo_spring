package com.dubuddy.DuBuddyInstitutes.dto;

import com.dubuddy.DuBuddyInstitutes.Entity.InsTestSeries;
import com.dubuddy.DuBuddyInstitutes.Entity.InsTestSeriesUserResponseBrief;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TestSeriesAndUserResponseDto {

    private InsTestSeries insTestSeries;
    private InsTestSeriesUserResponseBrief insTestSeriesUserResponseBrief;
}
