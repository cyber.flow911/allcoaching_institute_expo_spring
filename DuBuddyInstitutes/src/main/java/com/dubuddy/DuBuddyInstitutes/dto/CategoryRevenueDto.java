package com.dubuddy.DuBuddyInstitutes.dto;

import com.dubuddy.DuBuddyInstitutes.Entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CategoryRevenueDto {

    private Category category;
    private  Double todayRevenue;
    private  Double totalRevenue;


}
