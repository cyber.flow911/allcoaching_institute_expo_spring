package com.dubuddy.DuBuddyInstitutes.dto.CashFree.Order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@Data
@NoArgsConstructor
@ToString
public class Payments {
    private String url;
}
