package com.dubuddy.DuBuddyInstitutes.dto;

import lombok.*;

@ToString
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppStudentHomeDto {
    private String title;
    private String type;
    private long id;
    private Iterable data;

}
