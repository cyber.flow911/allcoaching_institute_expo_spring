package com.dubuddy.DuBuddyInstitutes.dto;

import com.dubuddy.DuBuddyInstitutes.Entity.Course;
import com.dubuddy.DuBuddyInstitutes.Entity.InsReview;
import com.dubuddy.DuBuddyInstitutes.Entity.Institute;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CategoryWisePurchaseDataDto {
    private InsReview insReview;
    private Institute institute;
    private Course course;
}
