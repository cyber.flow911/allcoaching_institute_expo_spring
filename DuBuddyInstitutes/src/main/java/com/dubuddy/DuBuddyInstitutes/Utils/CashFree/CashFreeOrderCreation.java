package com.dubuddy.DuBuddyInstitutes.Utils.CashFree;

import com.dubuddy.DuBuddyInstitutes.dto.CashFree.Order.CashFreeOrderDto;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@AllArgsConstructor

public class CashFreeOrderCreation {


    private static String SERVER_MODE_PRODUCTION = "PROD";
    private static String SERVER_MODE_DEBUG = "DEBUG";
    private static String SERVER_MODE_TEST = "TEST";
    static String apiKeyProd = "2769734f78e1d884712b39f9fa379672";

    static String apiKeyDebug = "22799121d26253f64310c0e691199722";

    static String apiSecretKeyProd = "e0482ff209e4507380d9b879df2556ed5c2e136a";
    static String apiSecretKeyDebug = "2bb25ae13343d6f73da3171752c62649ae7d185a";
    static String cashFreeBaseUrlSandbox = "https://sandbox.cashfree.com/";
    static String cashFreeBaseUrlProd = "https://api.cashfree.com/";

    public static String getCashFreeBaseUrl(boolean isModeProduction) {
        return isModeProduction ? cashFreeBaseUrlProd : cashFreeBaseUrlSandbox;
    }

    private static String getApiKey(boolean isModeProduction) {
        return isModeProduction ? apiKeyProd : apiKeyDebug;
    }

    private static String getApiSecretKey(boolean isModeProduction) {
        return isModeProduction ? apiSecretKeyProd : apiSecretKeyDebug;
    }

    public static CashFreeOrderDto createOder(String serverMode, float amount, String orderId, long userId, String name, String email, String mobile, String notifyUrl, String orderNote) {

        boolean isModeProduction = serverMode.equalsIgnoreCase(SERVER_MODE_PRODUCTION);


        String finalUrl = getCashFreeBaseUrl(isModeProduction) + "pg/orders";


        HttpURLConnection http = null;
        CashFreeOrderDto cashFreeOrderDto = new CashFreeOrderDto();
        try {

            URL url = new URL(finalUrl);
            http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("x-client-id", getApiKey(isModeProduction));
            http.setRequestProperty("x-client-secret", getApiSecretKey(isModeProduction));
            http.setRequestProperty("x-api-version", "2022-09-01");
            http.setRequestProperty("x-request-id", "DuBuddy_App_java_ins");

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("order_amount", amount);
            jsonObject.put("order_id", orderId);
            jsonObject.put("order_currency", "INR");
            JSONObject customerObj = new JSONObject();
            customerObj.put("customer_id", String.valueOf(userId));
            customerObj.put("customer_name", name);
            customerObj.put("customer_email", email);
            customerObj.put("customer_phone", mobile);
            jsonObject.put("customer_details", customerObj);
            JSONObject orderMataObj = new JSONObject();
            orderMataObj.put("notifyUrl", notifyUrl);
            jsonObject.put("order_meta", orderMataObj);
            jsonObject.put("order_note", orderNote);


            System.out.println(jsonObject);
            byte[] out = jsonObject.toString().getBytes(StandardCharsets.UTF_8);
            OutputStream stream = http.getOutputStream();
            stream.write(out);
            System.out.println(http.getResponseCode() + " " + http.getResponseMessage());

            if (http.getResponseMessage().equals("OK")) {
                BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {

                    content.append(inputLine);
                }
                in.close();
                System.out.println(content);
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                cashFreeOrderDto = objectMapper.readValue(content.toString(), CashFreeOrderDto.class);
                http.disconnect();

                return cashFreeOrderDto;

            } else {
                BufferedReader in = new BufferedReader(new InputStreamReader(http.getErrorStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {

                    content.append(inputLine);
                }
                in.close();
                System.out.println(content);
                http.disconnect();
                return cashFreeOrderDto;
            }


        } catch (IOException e) {
            e.printStackTrace();

        }
        return cashFreeOrderDto;
    }

    public static CashFreeOrderDto ValidateOrder(String serverMode, String orderId) {
        boolean isModeProduction = serverMode.equalsIgnoreCase(SERVER_MODE_PRODUCTION);


        String finalUrl = getCashFreeBaseUrl(isModeProduction) + "pg/orders/" + orderId;
        System.out.println(finalUrl);
        HttpURLConnection http = null;
        try {
            URL url = new URL(finalUrl);
            http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("GET");
            http.setDoOutput(true);
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("x-client-id", getApiKey(isModeProduction));
            http.setRequestProperty("x-client-secret", getApiSecretKey(isModeProduction));
            http.setRequestProperty("x-api-version", "2022-09-01");
            http.setRequestProperty("x-request-id", "DuBuddy_App_java_ins");

            System.out.println(http.getResponseCode() + " " + http.getResponseMessage());

            if (http.getResponseMessage().equals("OK")) {
                BufferedReader in = new BufferedReader(new InputStreamReader(http.getInputStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {

                    content.append(inputLine);
                }
                in.close();
                System.out.println(content);
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                CashFreeOrderDto cashFreeOrderDto = objectMapper.readValue(content.toString(), CashFreeOrderDto.class);
                http.disconnect();
                return cashFreeOrderDto;

            } else {
                http.disconnect();
                return new CashFreeOrderDto();
            }


        } catch (IOException e) {
            e.printStackTrace();

        }
        return new CashFreeOrderDto();
    }

    public static boolean RefundOrder(String serverMode, String orderId, float amount) {
        boolean isModeProduction = serverMode.equalsIgnoreCase(SERVER_MODE_PRODUCTION);
        String finalUrl = getCashFreeBaseUrl(isModeProduction) + "pg/orders/" + orderId + "/refunds";
        System.out.println(finalUrl);
        HttpURLConnection http = null;
        try {
            URL url = new URL(finalUrl);
            http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.setRequestProperty("Content-Type", "application/json");
            http.setRequestProperty("x-client-id", getApiKey(isModeProduction));
            http.setRequestProperty("x-client-secret", getApiSecretKey(isModeProduction));
            http.setRequestProperty("x-api-version", "2022-09-01");
            http.setRequestProperty("x-request-id", "DuBuddy_App_java_ins");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("refund_amount", amount);
            jsonObject.put("refund_id", orderId + "_refund");
            System.out.println(jsonObject);
            byte[] out = jsonObject.toString().getBytes(StandardCharsets.UTF_8);
            OutputStream stream = http.getOutputStream();
            stream.write(out);
            System.out.println(http.getResponseCode() + " " + http.getResponseMessage());

            if (http.getResponseMessage().equals("OK")) {
                BufferedReader in = new BufferedReader(new InputStreamReader(http.getErrorStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {

                    content.append(inputLine);
                }
                in.close();
                return true;

            } else {
                BufferedReader in = new BufferedReader(new InputStreamReader(http.getErrorStream()));
                String inputLine;
                StringBuilder content = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {

                    content.append(inputLine);
                }
                in.close();
                System.out.println(content);
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                RefundApiResponse refundApiResponse = objectMapper.readValue(content.toString(), RefundApiResponse.class);
                http.disconnect();
                return refundApiResponse.getCode().equalsIgnoreCase("order_id_not_paid");

            }


        } catch (IOException e) {
            e.printStackTrace();

        }
        return false;

    }
}


@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
 class RefundApiResponse{
    private String message;
    private String code;
    private String type;
}
