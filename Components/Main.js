import React from 'react';
import { Text, Dimensions, Platform, StyleSheet, View, TouchableOpacity, Keyboard, Image, StatusBar } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { screenMobileWidth, theme, appLogo } from './config'
import { connect } from 'react-redux';
import MobileViewController from './MobileViewController/MobileViewController'
import WebViewController from './WebViewController/WebViewController'
import MobileViewControllerIns from './MobileViewControllerIns/MobileViewControllerIns'
import WebViewControllerIns from './WebViewControllerIns/WebViewControllerIns'
import { screenWidthConfigChange, setInstituteAuth, userAuthStatus, setUserInfo, setInstituteDetails, setKeyboardHeight } from './Actions'
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('screen').height;
import AsyncStorage from '@react-native-async-storage/async-storage';

import { validateLogin } from './Utils/DataHelper/Coaching';
import Navigator from './Navigator';
import BottomSheetVideoPlayer from './BottomSheetVideoPlayer/BottomSheetVideoPlayer';
// import { StatusBar } from 'expo-status-bar';
import * as Linking from 'expo-linking';
import { linkingPrefixes } from './config';
const prefix = Linking.createURL('/');
import { navigationRef } from './RootNavigation';
import { NavigationContainer } from '@react-navigation/native';
const linking = {
    prefixes: [Linking.createURL('/'), ...linkingPrefixes],
    config: { 
        screens: {
            Institute: 'institute/:insId/:name', 
        },
    },
};
class Main extends React.Component {
    state = {
        width: windowWidth,
        height: windowHeight,
        mode: 1
    }

    _keyboardDidShow(e) {
        // this.props.navigation.setParams({
        //     keyboardHeight: e.endCoordinates.height,
        //     normalHeight: Dimensions.get('window').height, 
        //     shortHeight: Dimensions.get('window').height - e.endCoordinates.height, 
        // }); 
        let marginTop = Dimensions.get('window').height - e.endCoordinates.height;
        //   console.log("keyboard Open",marginTop," ",e.endCoordinates.height," ",Dimensions.get('window').height);
        this.props.setKeyboardHeight(e.endCoordinates.height)
    }
    _keyboardDidHide(e) {
        // this.props.navigation.setParams({
        //     keyboardHeight: e.endCoordinates.height,
        //     normalHeight: Dimensions.get('window').height, 
        //     shortHeight: Dimensions.get('window').height - e.endCoordinates.height, 
        // }); 
        //   // console.log("keyboard hidden");

        this.props.setKeyboardHeight(null)
    }
    componentWillUnmount() {
        if (this.keyboardDidShowListener) {
            this.keyboardDidShowListener.remove();
        }
        if (this.keyboardDidHideListener) {
            this.keyboardDidHideListener.remove();
        }
    }
    componentDidMount() {
        AsyncStorage.getItem('authInfo').then(data => {
            if (data) {
                data = JSON.parse(data)
                switch (data.authType) {
                    case 'ins':
                        validateLogin(data.email, data.password, (response) => {
                            if (response.status == 200) {
                                response.json().then(data => {
                                    if (data) {
                                        this.props.setInstituteDetails(data)
                                        this.props.setInstituteAuth(true);
                                        AsyncStorage.setItem('authInfo', JSON.stringify({ ...data, authType: 'ins' }))
                                    }
                                })
                            }
                        }, "non-hashed")
                        break;
                    case 'user':
                        this.props.userAuthStatus(true);
                        this.props.setUserInfo(data)
                        break;
                }
            }
        })
        this.props.screenWidthConfigChange(windowWidth)
        Dimensions.addEventListener('change', ({ window, screen }) => {
            this.setState({ width: window.width, height: screen.height })
            this.props.screenWidthConfigChange(window.width)
        });
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => this._keyboardDidShow(e));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', (e) => this._keyboardDidHide(2));
    }

    renderViewport = () => {
        return (<Navigator changeMode={this.changeMode} userAuth={this.props.userAuth} userInfo={this.props.userInfo} />)
    }

    switchRender = (mode) => {
        return (this.renderViewport());
    }
    changeMode = (mode) => {
        this.setState({ mode })
    }
    render() {
        return (
            <NavigationContainer
                ref={navigationRef}
                linking={linking} 
                fallback={<Text>Loading...</Text>}>
                <>
                    {/* {!this.props.statusBarHidden ? (<View style={{ height: StatusBar.currentHeight, width: "100%" }}>
                        <Text style={{ color: "#000", backgroundColor: '#fff' }}></Text>
                    </View>) : (null)} */}
                    <SafeAreaProvider style={[styles.safeAreaView, this.props.statusBarHidden ? { paddingTop: 0 } : null]}>
                        {this.switchRender(this.state.mode)}
                        {/* <AnimatedVideoPlayer/>   */}
                        {this.props.isVideoPlayerMounted?(
                            <BottomSheetVideoPlayer />
                        ):(null)}
                        
                    </SafeAreaProvider>
                </>
            </NavigationContainer>
        )
    }
}

const styles = StyleSheet.create({
    safeAreaView:
    {
        // paddingTop:Platform.OS=='android'?StatusBar.currentHeight:0
        // paddingTop:Platform.OS=='android'?0:0
    },
    container:
    {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoView:
    {
        flex: 0.1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerLogo:
    {
        height: 40,
        width: 40,
    },
    logoText:
    {
        fontSize: 20
    },
    btn:
    {
        flex: 0.2,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: '60%'
    },
    studentLoginBtn:
    {
        backgroundColor: theme.accentColor,
        padding: 10,
        borderRadius: 8,
        height: '25%',
        width: '100%',
        marginBottom: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    studentLoginText:
    {
        color: theme.primaryColor,
        fontSize: 18
    },
    instituteLoginBtn:
    {
        backgroundColor: theme.accentColor,
        padding: 10,
        borderRadius: 8,
        height: '25%',
        width: '100%',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    instituteLoginText:
    {
        color: theme.primaryColor,
        fontSize: 18
    }
})


const mapStateToProps = (state) => {
    return {
        userAuth: state.user.userAuthStatus,
        insAuth: state.institute.authStatus,
        userInfo: state.user.userInfo,
        statusBarHidden: state.screen.statusBarHidden,
        institute: state.institute,
        isVideoPlayerMounted: state.videoPlayer.isMounted

    }
}
export default connect(mapStateToProps, { setKeyboardHeight, screenWidthConfigChange, userAuthStatus, setInstituteAuth, setUserInfo, setInstituteDetails })(Main);