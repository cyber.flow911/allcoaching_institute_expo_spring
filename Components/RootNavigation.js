import { createNavigationContainerRef } from '@react-navigation/native';

export const navigationRef = createNavigationContainerRef()

export function navigate(name, params) {
  if (navigationRef?.isReady()) {
    navigationRef?.navigate(name, params);
  }
}

export function canGoBack() {
  if (navigationRef.isReady()) {
    navigationRef?.canGoBack();
  }
}
export function subscribeToState(callback) {    
    
    navigationRef?.addListener('state', callback)    
}
export function unsubscribeFromState(callback) {
    navigationRef?.removeListener('state', callback);
}
export function goBack() 
{
    if(navigationRef?.isReady())
    {
      navigationRef?.goBack();
    }
}
