import React, { useState, useRef, useEffect, forwardRef, useImperativeHandle } from 'react';
import { View, TouchableOpacity, ActivityIndicator, TouchableWithoutFeedback, Platform, Animated, AppState, Dimensions, StatusBar, BackHandler, Image, FlatList, ImageBackground } from 'react-native';
// import Video from 'react-native-video';
// import { Slider } from '@miblanchard/react-native-slider';
// import Orientation from 'react-native-orientation-locker';
import Slider from '@react-native-community/slider';
import * as ScreenOrientation from 'expo-screen-orientation'
import { DarkColoredText, loredText, RowLayout, SmallText } from '../GlobalStyle';
import { millisToMinutesAndSeconds, theme } from '../config';
import useStateRef from 'react-usestateref';
import { ButtonBackgroundView } from './Style';
import VideoPlayerBottomSheet, { SHEET_MODES } from './VideoSettings/VideoSettingsBottomSheet';
import * as NavigationBar from 'expo-navigation-bar';
import { Video, AVPlaybackStatus } from 'expo-av';
import { CONSTANTS } from '../config';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector } from 'react-redux';
const { width: windowWidth, height: windowHeight } = Dimensions.get('window');
const { width: screenWidth, height: screenHeight } = Dimensions.get('screen');
 
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { marginTop } from 'styled-system';
const navbarHeight = screenHeight - (windowHeight + StatusBar.currentHeight);

const VideoPlayer = forwardRef((props, ref) => {

    const {
        videoSettingsBottomSheetRef = useRef(),
        panHandlers = {},
        style = {}, source, poster, title = '', playList = [], autoPlay = false, playInBackground = false,
        showSeeking10SecondsButton = true, showHeader = true, showCoverButton = true, showFullScreenButton = true, showSettingButton = true, showMuteButton = true,
        enableControls = true,
        onOrientationChange = () => { },
        setPanResponderActiveState = () => { },
        isCollapsed = false,
        unMountVideoPlayer = () => { },
        showFull = () => { },
        showMini = () => { },
        startNextVideo = () => { },
    } = props

    const videoRef = useRef(null)
    const bottomSheetRef = useRef(null)
    const [isPaused, setIsPaused] = useState(!autoPlay)
    const [isMuted, setIsMuted] = useState(false)
    const [isVideoSeeked, setIsVideoSeeked] = useState(false)
    const [isVideoFocused, setIsVideoFocused] = useState(true)
    const [isVideoFullScreen, setIsVideoFullScreen, isVideoFullScreenRef] = useStateRef(false)
    const [isErrorInLoadVideo, setIsVErrorInLoadVideo] = useState(false)
    const [showNextVideoLoader, setShowNextVideoLoader] = useState(false)
    const [isVideoEnd, setIsVideoEnd] = useState(false)
    const [isVideoCovered, setIsVideoCovered] = useState(false)
    const [videoDuration, setVideoDuration] = useState(0)
    const [videoQuality, setVideoQuality] = useState(144)
    const [videoSound, setVideoSound] = useState(1.0)
    const [currentVideoDuration, setCurrentVideoDuration] = useState(0)
    const [currentVideoDurationDisplayValue,setCurrentVideoDurationDisplayValue, currentVideoDurationDisplayValueRef] = useStateRef(0)
    const [isVideoLoaded, setIsVideoLoaded] = useState(false)
    const [videoRate, setVideoRate] = useState(1)
    const [loopCurrentVideo, setLoopCurrentVideo] = useState(false)
    const [playlistSelectedVideo, setPlaylistSelectedVideo] = useState(null) 
    const portraitStyle = { alignSelf: 'center', height: 200, width: windowWidth, ...style }
    const landScapeStyle = { alignSelf: 'center', height: screenWidth, width:screenHeight  }
    const videoStyle = isVideoFullScreen ? landScapeStyle : portraitStyle
    const videoData = useSelector(state => state.videoPlayer.videoData)
    const [nextVideoCounter, setNextVideoCounter, nextVideoCounterRef] = useStateRef(10)
    const intervalRef = useRef(null)
    const [resettingSlider, setResettingSlider] = useState(false)
    const [slidingStarted, setSlidingStarted] = useState(false)
    useEffect(() => {
        setIsVideoLoaded(false)
    }, [videoData])
    const hideNavigationBar = async () => {
        // await NavigationBar.setPositionAsync('absolute')
        await NavigationBar.setBackgroundColorAsync('#ffffff00')
        NavigationBar.setVisibilityAsync("hidden");

    }
    const showNavigationBar = async () => {
        // await NavigationBar.setPositionAsync('relative')
        await NavigationBar.setBackgroundColorAsync('#000000')
        NavigationBar.setVisibilityAsync("visible");
    }
    const onVideoEnd = () => {
        
        
        if (loopCurrentVideo) {
            // videoRef.current.setPositionAsync(0)
            setCurrentVideoDuration(0)
            setCurrentVideoDurationDisplayValue(0)
            setIsVideoEnd(false)
            setIsPaused(false)
            setIsVideoSeeked(false)
        } else {
            setIsVideoEnd(true)
            setIsPaused(true)
            const nextVideo  =startNextVideo()
            console.log('nextVideo', nextVideo)
            if (nextVideo) { 
                setCurrentVideoDuration(0)
                setCurrentVideoDurationDisplayValue(0)
                // setShowNextVideoLoader(true)
                setIsVideoEnd(false)
                setIsPaused(false) 
                // intervalRef.current = setInterval(() => {
                //     if (nextVideoCounterRef.current > 0) {
                //         setNextVideoCounter(nextVideoCounterRef.current - 1)
                //     } else {
                //         setNextVideoCounter(5)
                //         setIsVideoEnd(false)
                //         setIsPaused(false) 
                //         // videoRef.current.setPositionAsync(0)
                //         setShowNextVideoLoader(false)
                //         clearInterval(intervalRef.current)
                //     }
                // }, 1000)
            }
        }
    }
    useEffect(() => { 
        const backHandlerSubscriber = BackHandler.addEventListener('hardwareBackPress', () => {
            saveVideoSeenDuration()
            console.log('hardwareBackPress ' + isCollapsed," isCollapsed "," isVideoFullScreen ",isVideoFullScreen)
            if (isVideoFullScreen) {
                exitFullScreen()
                return true
            }
            else if(!isCollapsed)
            {
                showMini()
                return true
            }  
                
            return false
        })
        return () => { 
            backHandlerSubscriber?.remove()
            // showNavigationBar()
            saveVideoSeenDuration()
        }
    }, [isVideoFullScreen, isCollapsed])

    useEffect(() => {
        const appStateSubscriber = AppState.addEventListener('change', (state) => {
            // saveVideoSeenDuration()
            if (playInBackground && isPaused == false) {
                setIsPaused(false)
            } else {
                setIsPaused(true)
            }
        })

        if (isPaused) {
            videoRef.current.pauseAsync()
        } else {

            videoRef.current.playAsync();
        }

        return () => {
            appStateSubscriber?.remove()
        }
    }, [isPaused])

    useImperativeHandle(ref, () => ({
        toggleVideoPlay: () => setIsPaused(!isPaused),
        changeOrientationToPortait: () => {
            exitFullScreen()
        },
        setVideoSeekTime: (time) => {
            
            setCurrentVideoDuration(time)
            setCurrentVideoDurationDisplayValue(time)
        },
        setVideoRate: (rate) => {
            setVideoRate(rate)
        },
        setLoopCurrentVideo: (loop) => {
            setLoopCurrentVideo(loop)
        },
        setVideoQuality: (quality) => {
            setVideoQuality(quality)
        }
    }))



    const goFullScreen = () => {
        // Orientation.lockToLandscape()
        // setDimension({ width: screenHeight, height: screenWidth })

        
        onOrientationChange('LANDSCAPE')
        ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE)
        setIsVideoFullScreen(true)
        StatusBar.setHidden(true)
        hideNavigationBar()
    }
    const exitFullScreen = () => {
        // Orientation.lockToPortrait()
        setIsVideoFullScreen(false)
        // setDimension({ width:screenWidth , height: screenHeight })
        onOrientationChange('PORTRAIT')
        ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT)
        StatusBar.setHidden(false)
        showNavigationBar()
    }
    const startFromPreviouslySeenDuration = async () => {
        const value = await AsyncStorage.getItem('videoDuration')
        if (value) {
            const parsedValue = JSON.parse(value)
            if (parsedValue[videoData.id]) {
                setCurrentVideoDuration(parsedValue[videoData.id])
                setCurrentVideoDurationDisplayValue(parsedValue[videoData.id])
            }
        }
    }
    const saveVideoSeenDuration = async () => {
        const value = await AsyncStorage.getItem('videoDuration')
        if (value) {
            const parsedValue = JSON.parse(value)
            parsedValue[videoData.id] = currentVideoDuration
            AsyncStorage.setItem('videoDuration', JSON.stringify(parsedValue))
        } else {
            AsyncStorage.setItem('videoDuration', JSON.stringify({ [videoData.id]: currentVideoDuration }))
        }
    }
    const videoHeaders = () => (
        <TouchableWithoutFeedback activeOpacity={1} onPress={() => { }}>
            <RowLayout style={{ alignItems: "flex-start", alignSelf: "flex-start", width: "100%", justifyContent: 'space-between', paddingHorizontal: isVideoFullScreen ? 5 : 10,paddingRight:isVideoFullScreen?20:0 }} >
                <RowLayout>
                    <TouchableOpacity onPress={() => {
                        if (isVideoFullScreenRef.current) {
                            exitFullScreen()
                        } else {
                            showMini()
                        }
                    }}>
                        <MaterialCommunityIcons onPress={() => {
                            if (isVideoFullScreenRef.current) {
                                exitFullScreen()
                            } else {
                                showMini()
                            }
                        }} name="chevron-down" size={CONSTANTS.NORMAL_ICON_SIZE - 8} color={theme.primaryColor} />
                    </TouchableOpacity>
                    {isVideoFullScreen ? (
                        <DarkColoredText numberOfLines={1} fontFamily={"Raleway_700Bold"} size={"18px"} style={{ color: theme.primaryColor }}>{playlistSelectedVideo ? playlistSelectedVideo.title ? playlistSelectedVideo.title : '' : title}</DarkColoredText>
                    ) : (
                        <View />
                    )}
                </RowLayout>
                <RowLayout style={{ justifyContent: 'space-between' }} >
                    {showSettingButton &&
                        <TouchableOpacity
                            onPress={() => {

                                // alert("hello")
                                // bottomSheetRef.current?.sheet.close()
                                videoSettingsBottomSheetRef.current?.setHeightFactor(0.8)
                                videoSettingsBottomSheetRef.current?.setMode(SHEET_MODES.SETTINGS)
                                setTimeout(() => {
                                    videoSettingsBottomSheetRef.current?.sheet.show()
                                }, 100)
                            }}
                            style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <MaterialIcons onPress={() => {
                                videoSettingsBottomSheetRef.current?.setHeightFactor(0.8)
                                videoSettingsBottomSheetRef.current?.setMode(SHEET_MODES.SETTINGS)
                                setTimeout(() => {
                                    videoSettingsBottomSheetRef.current?.sheet.show()
                                }, 100)
                            }} name={"settings"} size={CONSTANTS.NORMAL_ICON_SIZE - 12} color={theme.primaryColor} />
                        </TouchableOpacity>
                    }
                    {showCoverButton &&
                        <TouchableOpacity
                            onPress={() => {
                                setIsVideoCovered(true)
                                setIsVideoFocused(false)
                            }}
                            style={{ marginRight: 10 }}
                        >
                            <MaterialIcons name="remove-red-eye" size={CONSTANTS.NORMAL_ICON_SIZE - 8} color={theme.primaryColor} />
                        </TouchableOpacity>
                    }
                </RowLayout>
            </RowLayout>
        </TouchableWithoutFeedback>
    )

 
    const videoFooter = () => (
        <TouchableOpacity style={{ width: "100%" }} onPress={() => { }} activeOpacity={1}>
            <View style={{ width: "100%", paddingTop: isVideoFullScreen ? 30 : 0 }} >
                <RowLayout style={{ justifyContent: "space-between", paddingHorizontal: isVideoFullScreen ? 20 : 10, marginTop: 5, marginBottom: 0 }}>
                    <DarkColoredText style={{ color: theme.primaryColor }} size="12px">
                        {millisToMinutesAndSeconds(currentVideoDurationDisplayValue)} /
                        <DarkColoredText size="12px" style={{ color: theme.labelOrInactiveColor, }} > {millisToMinutesAndSeconds(videoDuration)}</DarkColoredText>
                    </DarkColoredText>
                    <RowLayout style={{ justifyContent: "space-evenly" }}>
                        {showMuteButton &&
                            <TouchableOpacity
                                onPress={() => {
                                    setIsMuted(!isMuted)
                                    if (isMuted && videoSound == 0) {
                                        setVideoSound(1.0)
                                    }
                                }}
                                style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center' }}
                            >
                                <Image source={isMuted ? require('./images/muteSound.png') : require('./images/fullSound.png')} style={{ width: 22, height: 22, }} />
                            </TouchableOpacity>
                        }
                        {showFullScreenButton &&
                            <TouchableOpacity
                                onPress={() => {
                                    if (isVideoFullScreenRef.current) {
                                        exitFullScreen()
                                    } else {
                                        goFullScreen()
                                    }
                                }}
                                style={{ width: 30, height: 30, justifyContent: 'center', alignItems: 'center', }}
                            >
                                <MaterialIcons name={isVideoFullScreen ? "fullscreen-exit" : "fullscreen"} size={CONSTANTS.NORMAL_ICON_SIZE - 8} color="white"
                                />
                            </TouchableOpacity>
                        }
                    </RowLayout>
                </RowLayout>
 
                {isVideoLoaded?(
                <Slider
                    maximumValue={videoDuration}
                    minimumValue={0}
                    minimumTrackTintColor={theme.primaryColor}
                    maximumTrackTintColor={theme.purpleColor}
                    thumbTintColor={theme.primaryColor}
                    style={{ height: 10, marginLeft: isVideoFullScreen ? -20 : -10, width: isVideoFullScreen ? videoStyle.width : videoStyle.width + 10, marginBottom: 10, alignSelf: "flex-start", alignItems: "flex-start", justifyContent: "flex-start" }} 
                    useNativeDriver
                    tapToSeek={false}
                    onValueChange={(sliderData) => {
                        // setCurrentVideoDuration(sliderData)
                        // setIsVideoFocused(true)
                    }} 
                    // value={currentVideoDurationDisplayValue}
                    {...(slidingStarted ? {   } : { value: currentVideoDurationDisplayValue })}
                    onSlidingStart={() => {
                        setSlidingStarted(true)
                    }} 
                    onSlidingComplete={sliderData => {
                        setTimeout(() => {
                            setSlidingStarted(false)
                        },1000)
                        // setSlidingStarted(false)
                        setCurrentVideoDuration(sliderData)
                        setCurrentVideoDurationDisplayValue(sliderData)
                        // videoRef.current.setPositionAsync(sliderData)
                    }}
                />):(
                    <View style={{height:20}}/>
                )}
                
            </View>
        </TouchableOpacity>
    )

    const videoSeekingIncreaseButton = () => (
        <ButtonBackgroundView
            style={{ justifyContent: 'center', alignItems: 'center', }}
            onPress={() => {
                // videoRef.current.setPositionAsync(currentVideoDuration + 10000)

                
                setCurrentVideoDuration(currentVideoDurationDisplayValueRef.current + 10000)
                setCurrentVideoDurationDisplayValue(currentVideoDurationDisplayValueRef.current + 10000)
            }}
        >
            <MaterialIcons name="forward-10" size={25} color="white" />
        </ButtonBackgroundView>
    )

    const videoPlayPauseButton = () => (
        <ButtonBackgroundView
            style={{ justifyContent: 'center', alignItems: 'center', }}
            onPress={videoPlayPause}
        >

            <MaterialIcons name={isPaused ? "play-arrow" : "pause"} size={30} color="white" />
        </ButtonBackgroundView>




    )
    const videoSeekingDecreaseButton = () => (
        <ButtonBackgroundView
            style={{ justifyContent: 'center', alignItems: 'center', }}
            onPress={() => {
                // videoRef.current.setPositionAsync(currentVideoDuration - 10000)

                setCurrentVideoDuration(currentVideoDurationDisplayValueRef.current - 10000)
                setCurrentVideoDurationDisplayValue(currentVideoDurationDisplayValueRef.current - 10000)
            }}
        >

            <MaterialIcons name={"replay-10"} size={25} color="white" />
        </ButtonBackgroundView>
    )

    const videoPlayPause = () => {
        if (isVideoEnd) {
            // videoRef.current.setPositionAsync(0)
            setIsVideoEnd(false)
            setCurrentVideoDuration(0)
            setCurrentVideoDurationDisplayValue(0)
            setIsPaused(false)
        } else {
            setIsPaused(!isPaused)
        }
    }






    const videoSeekedLoader = () => (
        <View style={{ height: videoStyle.height, width: videoStyle.width, position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(0 ,0, 0,0.5)', justifyContent: 'center', alignItems: 'center' }} >
            <ActivityIndicator color='white' size='large' />
        </View>
    )

    const videoErrorView = () => (
        <View style={{ height: videoStyle.height, width: videoStyle.width, position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(0 ,0, 0,0.5)', justifyContent: 'center', alignItems: 'center' }} >
            <Image source={require('./images/error.png')} style={{ width: 30, height: 30, }} />
            <DarkColoredText style={{ color: 'white', fontSize: 12, marginTop: 0 }} >Error when load video</DarkColoredText>
        </View>
    )

    const videoPosterView = () => (
        <Image
            source={{ uri: playlistSelectedVideo ? playlistSelectedVideo.poster ? playlistSelectedVideo.poster : '' : poster }}
            style={{ height: videoStyle.height, width: videoStyle.width, position: 'absolute', top: 0, left: 0, backgroundColor: 'rgba(0 ,0, 0,0.5)', }}
        />
    )


    const videoControlsMiddleRow = () => (
        <RowLayout
            style={{ width: "100%", justifyContent: 'space-evenly', height: videoStyle.height - (isVideoFullScreen ? 130 : 75), alignItems: 'center', }}
        >
            {(isVideoFocused && showSeeking10SecondsButton && !isErrorInLoadVideo) && videoSeekingDecreaseButton()}
            {(isVideoFocused) && videoPlayPauseButton()}
            {(isVideoFocused && showSeeking10SecondsButton && !isErrorInLoadVideo) && videoSeekingIncreaseButton()}

        </RowLayout>
    )



    const nextVideoLoadWaitView = () => {
        return (
            <View style={{ alignItems: "center" }}>
                <View style={{ alignItems: "center" }}>
                    <DarkColoredText size={'12px'} color={theme.primaryColor}>
                        Up next
                    </DarkColoredText>
                    <DarkColoredText style={{ marginVertical: 5 }} color={theme.primaryColor} numberOfLines={1}>
                        {videoData?.title}
                    </DarkColoredText>
                </View>
                <View>
                    {/* <View style={{ position: "absolute", backgroundColor: theme.secondaryColor, borderRadius: 25 }}>
                        <Progress.Circle
                            size={45}
                            thickness={3}
                            borderWidth={0}
                            unfilledColor={theme.secondaryColor + "66"}
                            color={theme.primaryColor}
                            progress={(10 - nextVideoCounterRef.current) / 10}
                        />
                    </View> */}
                    <View>
                        <TouchableOpacity
                            style={{
                                borderRadius: 50,
                                padding: 5,
                                justifyContent: 'center', alignItems: 'center',
                            }}
                            activeOpacity={0.8}
                            onPress={() => {
                                clearInterval(intervalRef)
                                setIsPaused(false)
                                setIsVideoEnd(false)
                                setCurrentVideoDuration(0)
                                setCurrentVideoDurationDisplayValue(0)
                                setShowNextVideoLoader(false)
                            }}
                        >
                            <MaterialIcons name={isPaused ? "skip-next" : "pause"} size={35} color="white" />
                        </TouchableOpacity>
                    </View>
                </View>
                <View>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            console.log("cancel")
                            clearInterval(intervalRef)
                            setIsPaused(true)
                            setIsVideoEnd(true)
                        }}
                    >
                        <DarkColoredText style={{ marginVertical: 10 }} color={theme.primaryColor} numberOfLines={1}>
                            Cancel
                        </DarkColoredText>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        )

    }
    const videoCoverView = () => (
        <View
            style={{ position: 'absolute', paddingBottom: isVideoFullScreen ? 20 : 10, paddingHorizontal: isVideoFullScreen ? 35 : 0, top: 0, paddingTop: 20, left: 0, alignItems: 'center', justifyContent: 'center', ...videoStyle, height: '100%', width: "100%", backgroundColor: 'rgba(0 ,0, 0,0.35)' }}
        >
            {showNextVideoLoader ? (
                <>
                    <View></View>
                    <View>
                        {nextVideoLoadWaitView()}
                    </View>
                    <View></View>

                </>
            ) : (
                <>
                    {(isVideoFocused && showHeader) && videoHeaders()}
                    {isVideoFocused && videoControlsMiddleRow()}
                    {isVideoFocused && videoFooter()}
                </>
            )}

        </View> 
    )



    return (
        <View
            {...panHandlers}
            style={[isCollapsed ? { flexDirection: "row", justifyContent: "space-between", width: "100%", } : {}]}>
            <TouchableOpacity
                onPress={() => {
                    if (isCollapsed) {
                        showFull()
                    } else {
                        setIsVideoFocused(!isVideoFocused)
                    }
                }}
                activeOpacity={1}
                style={{ backgroundColor: theme.secondaryColor, }}
            >
                <View style={videoStyle} >
                    <Video
                        style={{ flex: 1 }}
                        repeat={loopCurrentVideo}
                        posterResizeMode='contain'
                        resizeMode='contain'
                        bufferConfig={{
                            minBufferMs: 1000 * 60,
                            bufferForPlaybackMs: 1000 * 60,
                            bufferForPlaybackAfterRebufferMs: 1000 * 60,
                        }}
                        ref={videoRef}
                        source={source}
                        paused={isPaused}
                        muted={isMuted}
                        rate={videoRate}
                        shouldCorrectPitch={true}
                        selectedVideoTrack={{
                            type: 'resolution',
                            value: videoQuality
                        }}
                        volume={videoSound}
                        playInBackground={playInBackground}
                        onLoadStart={() => {
                            setIsVideoSeeked(true)
                            setIsVErrorInLoadVideo(false)
                        }}
                        onLoad={videoData => { 
                            setIsVideoSeeked(false)
                            setIsVideoLoaded(true)
                            // startFromPreviouslySeenDuration()
                            setVideoDuration(videoData.durationMillis)
                            setIsVErrorInLoadVideo(false)
                        }}
                        onBuffer={videoData => {
                            setIsVideoSeeked(true)
                        }}
                        progressUpdateIntervalMillis={900}
                        positionMillis={currentVideoDuration}
                        onPlaybackStatusUpdate={videoData => { 
                            if (videoData.isPlaying) {
                                setIsVideoSeeked(false)
                            } 
                            if(Math.abs(videoData.positionMillis-currentVideoDurationDisplayValueRef.current)<1300)
                            {
                                setCurrentVideoDurationDisplayValue(videoData.positionMillis)
                            }
                            
                            if (videoData.didJustFinish) {
                                onVideoEnd()
                            }
                        }}
                        
                        onReadyForDisplay={() => {
                            setIsVideoSeeked(false)
                            setIsVErrorInLoadVideo(false)
                        }}
                        onError={(videoData) => { setIsVideoSeeked(false); setIsVErrorInLoadVideo(true) }}
                    />
                    {(currentVideoDurationDisplayValue == 0 && poster) && videoPosterView()}
                    {isVideoSeeked && videoSeekedLoader()}
                    {isErrorInLoadVideo && videoErrorView()}
                    {isVideoFocused && enableControls && videoCoverView()} 
                </View>
            </TouchableOpacity> 
            {isCollapsed ? (
                <TouchableWithoutFeedback
                    onPress={() => {
                        showFull()
                    }}
                >
                    <>
                        <View style={{ flex: 0.7, marginHorizontal: 5, marginBottom: 10, justifyContent: "center" }}>
                            <DarkColoredText numberOfLines={1} >{title}</DarkColoredText>
                            <SmallText numberOfLines={1}>{videoData.instituteDetails?.name}</SmallText>
                        </View>
                        <RowLayout style={{ flex: 0.5, marginBottom: 10, justifyContent: "space-evenly" }}>
                            <TouchableWithoutFeedback onPress={videoPlayPause}>
                                <MaterialIcons name={isPaused ? "play-arrow" : "pause"} size={CONSTANTS.NORMAL_ICON_SIZE} color="black" />
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback onPress={unMountVideoPlayer}>
                                <View>
                                    <MaterialIcons name="close" size={CONSTANTS.NORMAL_ICON_SIZE} color="black" />
                                </View>
                            </TouchableWithoutFeedback>
                        </RowLayout>
                    </>
                </TouchableWithoutFeedback>
            ) : (null)}
            {/* {(!isVideoFullScreen && !isCollapsed) ? (

                <View
                >
                    <Slider
                        maximumValue={videoDuration}
                        minimumValue={0}
                        minimumTrackTintColor={theme.primaryColor}
                        maximumTrackTintColor={theme.purpleColor}
                        thumbTintColor={theme.primaryColor}
                        style={{height:10,width:videoStyle.width,padding:0,margin:0,}}
                        // trackStyle={isVideoFocused ? { height: 4, width: "100%" } : { height: 1, width: "100%" }}
                        useNativeDriver
                        tapToSeek
                        // trackClickable
                        thumbTouchSize={{ width: 50, height: 50 }}
                        onValueChange={(sliderData) => {
                            // setCurrentVideoDuration(sliderData)
                            // setIsVideoFocused(true)
                        }}
                        // thumbStyle={!isVideoFocused ? { height: 0, width: 0 } : { height: 15, width: 15, }}
                        // containerStyle={isVideoFocused ? { width: "100%", height: 4, } : { width: "100%", height: 1.5, }}
                        value={currentVideoDuration}
                        onSlidingComplete={sliderData => {
                            setCurrentVideoDuration(sliderData)
                            // videoRef.current.setPositionAsync(sliderData)
                        }}
                    />
                </View>
            ) : (null)} */} 
        </View>
    );
})

export default VideoPlayer

