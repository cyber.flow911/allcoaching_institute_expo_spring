import { View, Text, StyleSheet, FlatList, TouchableOpacity } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import { Assets, CONSTANTS, dataLimit, theme } from '../../config';
// import { fetch_courses_videos_with_hidden, fetch_video_playlist } from '../../Apis/Node/InstituteCourse';
// import { theme } from '../../theme';

import RenderVideo from './InstituteVideoView';
import { useDispatch, useSelector } from 'react-redux';
// import EmptyScreen from '../EmptyScreen/EmptyScreen';
// import { SET_COURSE_ACTIVE_VIDEO_PLAYLIST, SET_INSTITUTE_COURSE_VIDEOS, SET_INSTITUTE_COURSE_VIDEO_PLAYLISTS } from '../../Redux/Types';
// import Loader from '../Loader/Loader';
// import { saveStudentHistory } from '../../Apis/Node/InstituteView';
// import CoursePurchaseBottomSheet from '../CoursePurchaseBottomSheet/CoursePurchaseBottomSheet';
import useStateRef from 'react-usestateref';
import { EvilIcons } from '@expo/vector-icons';
import EmptyList from '../../Utils/EmptyList';
import { saveStudentHistory } from '../../Utils/DataHelper/StudentHistory';
import { fetch_courses_videos_with_hidden, fetch_video_playlist } from '../../Utils/DataHelper/Course';
import { SET_COURSE_ACTIVE_VIDEO_PLAYLIST, SET_INSTITUTE_COURSE_VIDEOS, SET_INSTITUTE_COURSE_VIDEO_PLAYLISTS } from '../../Actions/types';
import CustomActivtiyIndicator from '../../Utils/CustomActivtiyIndicator';

const VIDEO_HIDDEN_STATUS_FALSE = false

const VideoPlayerCourseVideosView = ({ renderExtraFooterSpace }) => {
  const activeCourseDetails = useSelector(state => state.institute.activeCourseDetails)
  const activeCourse = activeCourseDetails.id;
  const userDetails = useSelector(state => state.user.userInfo);
  const [offset, setOffset, offsetRef] = useStateRef(0);
  const videoPlaylists = useSelector(state => state.institute.videoPlaylists[activeCourse]);
  const [showLoadMore, setShowLoadMore] = useState(true);
  const [loading, setLoading] = useState(true);
  const [loadingMore, setLoadingMore] = useState(false)
  const [loadingPlaylist, setLoadingPlaylist] = useState(true)
  const activePlaylist = useSelector(state => state.institute.videoActivePlaylist[activeCourse]) || -1;
  const DATA_KEY = activeCourse + "_" + activePlaylist
  const videoData = useSelector(state => state.videoPlayer.videoData)
  const videos = useSelector(state => state.institute.courseVideos[DATA_KEY]);
  const purchaseBottomSheetRef = useRef(null);
  const dispatch = useDispatch(); 
  const studentEnrolled = useSelector(state => state.institute.userEnrolledCourses[activeCourse]);
  const openPurchaseCourseModal = () => {
    purchaseBottomSheetRef.current?.show();
  }

  useEffect(() => {
    getVideoPlaylists();
  }, [])

  useEffect(() => {
    getCourseVideos();
  }, [offset, activeCourse, activePlaylist])

  useEffect(() => {
    if (videos?.length == 0) {
      setLoading(true)
    } else {
      setLoading(false)
    }
  }, [videos])

  const changePlaylist = (playlist) => {
    dispatch({ type: SET_COURSE_ACTIVE_VIDEO_PLAYLIST, payload: { courseId: activeCourse, videoActivePlaylist: playlist.id } })
    setOffset(0);
  }

  const getCourseVideos = async () => {
    fetch_courses_videos_with_hidden(VIDEO_HIDDEN_STATUS_FALSE, offsetRef.current, dataLimit, activeCourse, (response) => { 
      if (response.status == 200) {
        response.json().then((data) => {
          if (offsetRef.current == 0) {
            dispatch({ type: SET_INSTITUTE_COURSE_VIDEOS, payload: { courseId: DATA_KEY, courseVideos: data } })
          } else {
            dispatch({ type: SET_INSTITUTE_COURSE_VIDEOS, payload: { courseId: DATA_KEY, courseVideos: [...videos, ...data] } })
          }
          if (data.length<dataLimit) {
            setShowLoadMore(false);
          } else {
            setShowLoadMore(true);
          }
        })
      }
    }, activePlaylist);

    setLoading(false);
    setLoadingMore(false);
  }

  const getVideoPlaylists = async () => {
    fetch_video_playlist(activeCourse, (response) => {
      if (response.status == 200) {
        response.json().then((data) => {
          const playlist = { "courseId": activeCourse, "id": -1, "name": "All" }
          data.unshift(playlist)
          dispatch({ type: SET_INSTITUTE_COURSE_VIDEO_PLAYLISTS, payload: { courseId: activeCourse, videoPlaylists: data } })
        })
      }
    });
    setLoadingPlaylist(false)
    setLoading(false)
    setLoadingMore(false)

  }

  const renderPlaylist = ({ item }) => {
    return (
      <TouchableOpacity
        onPress={() => { changePlaylist(item) }}
        style={[styles.singleSubject, activePlaylist == item.id ? ({ backgroundColor: theme.secondaryColor }) : (null)]}>
        <Text style={[styles.singleSubjectText, activePlaylist == item.id ? ({ color: theme.primaryColor }) : (null)]}>{item.name}</Text>
      </TouchableOpacity>
    )
  }
  const addToHistory = async (type, id) => {
    saveStudentHistory(type, id, userDetails.id, (response) => { })
  }


  return (
    <View>
      <View style={styles.AddFilter}>
        {/* {loadingPlaylist?(<CustomActivtiyIndicator mode={"video"}/>):( */}
        <FlatList
          data={videoPlaylists}
          renderItem={renderPlaylist}
          keyExtractor={(item) => item.id}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
        />
        {/* )} */}
      </View>
      <View style={styles.dataContainer}>
        {loading ? (
          <CustomActivtiyIndicator mode={"video"} />
        ) : (
          <FlatList
            pointerEvents='box-only'
            data={videos}
            renderItem={({ item, index }) => <RenderVideo
              item={item}
              addToHistory={addToHistory}
              mode="student"
              studentEnrolled={studentEnrolled}
              downloadMode={true}
              action={[]}
              openPurchaseCourseModal={openPurchaseCourseModal}
              isBeingPlayed={item?.id == videoData?.id}
            />}
            keyExtractor={(item) => item.id}
            showsVerticalScrollIndicator={false}
            horizontal={false}
            onEndReachedThreshold={0.5}
            onEndReached={() => {
              if (showLoadMore && !loadingMore) {
                setLoadingMore(true);
                setOffset(offsetRef.current + 1)
              }
            }}
            showsHorizontalScrollIndicator={false}
            ListEmptyComponent={<EmptyList image={Assets.noResult.noRes1} />}
            ListFooterComponent={() => {
              return (
                <View style={{ height: 400 }} />
              )
            }}
          />
        )}
        {loadingMore ? (
          <CustomActivtiyIndicator mode={"video"} />
        ) : (null)}
      </View>
      {showLoadMore ? (
        <TouchableOpacity style={[styles.loadMoreView]} onPress={() => setOffset(offset + 1)}>
          <View style={{}}><EvilIcons name="chevron-down" size={20} /></View>
          <Text style={{ margin: 5 }}>Load More</Text>
        </TouchableOpacity>
      ) : (null)}

      {/* <CoursePurchaseBottomSheet
                ref={purchaseBottomSheetRef} 
                instituteId={activeCourseDetails.instId} 
            /> */}
    </View>
  )
}



const styles = StyleSheet.create({
  singleSubject:
  {
    marginLeft: 5,
    borderWidth: 1,
    paddingHorizontal: 10,
    borderColor: theme.greyColor,
    borderRadius: 20,
  },
  singleSubjectText:
  {
    marginLeft: 6,
    marginRight: 6,
    paddingLeft: '4%',
    paddingRight: '4%',
    paddingTop: 4,
    paddingBottom: 4,
    fontSize: 12,
    color: theme.greyColor,
  },
  dataContainer:
  {

    display: 'flex',
    flexDirection: 'column'
  },
  loadMoreView:
  {

    flexDirection: 'row',
    justifyContent: 'center',
    margin: 5,
    borderRadius: 15,

    alignSelf: 'center',
    backgroundColor: theme.secondaryColor + '4D',
    padding: 5,
    alignItems: 'center',

    flexWrap: 'wrap'
  },
})
export default VideoPlayerCourseVideosView
