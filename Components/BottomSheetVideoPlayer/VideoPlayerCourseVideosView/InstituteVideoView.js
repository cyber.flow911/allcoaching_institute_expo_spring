
/* ---------------------------- will see later on --------------------------- */
// import { downloadFile, pauseDownload } from '../Utils/DownloadFile';
// import { setDownloadingItem,setDownloadingItemProgress,removeDownloadingItem } from '../Actions';
// import CircularProgress from 'react-native-circular-progress-indicator';
// import Arrow_down_circle_black from '../Utils/Icons/Arrow_down_circle_black';
/* -------------------------------- resolved -------------------------------- */
import React, { useEffect, useRef, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, FlatList, Image, Platform, ScrollView, Dimensions, findNodeHandle, UIManager, Modal, TouchableWithoutFeedback } from 'react-native';
import { connect, useDispatch, useSelector } from 'react-redux'
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import { fileBaseUrl, imageProvider, numFormatter, serverBaseUrl, theme } from '../../config';
import { Entypo } from '@expo/vector-icons';
import BlinkView from '../../Utils/BlinkView';
import { DarkColoredText } from '../../GlobalStyle';
import { SET_VIDEO_PLAYER_DATA, TOGGLE_VIDEO_PLAYER_MOUNT_STATE } from '../../Actions/types';



const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const RenderVideo=({videoType,item,studentEnrolled,addToHistory,mode,downloadMode,openPurchaseCourseModal,isBeingPlayed})=>{
    
    // const [savingItem, setSavingItem] = useState(props.savingItem ? props.savingItem : false)
    const [downloadProgress, setDownloadProgress] = useState(0)
    const dispatch = useDispatch()
    const instituteDetails = useSelector(state => state.institute.instituteDetails)
    const pauseDownloadRef = useRef()
    // download = (item, type) => {
    //     Toast.show('PLease Wait...')


    //     downloadFile(item, item.videoLocation, props.user.id, type, downloadCallback, downloadProgessCallback, (offlineItem) => setDownloadingItemInRedux(offlineItem, item.videoLocation))
    // }
    // setDownloadingItemInRedux = (offlineItem, videoLocation) => {

    //     setState({ downloadRef: offlineItem.downloadRef, savingItem: true })

    //     props.setDownloadingItem({ ...offlineItem, url: videoLocation, type: 'video' }, 0);
    // }
    // cancelDownload = () => {
    //     pauseDownloadRef.current = true
    //     pauseDownload({ downloadRef: state.downloadRef }, () => {
    //         removeDownloadingItem(props.videoLocation)
    //         pauseDownloadRef.current = false
    //     })
    // }
    // downloadProgessCallback = (progress, url) => {
    //     props.setDownloadingItemProgress(progress, url)
    //     if (progress >= 100) {
    //         props.removeDownloadingItem(url)
    //     }
    //     setState({ downloadProgress: progress })
    // }
    // downloadCallback = (response) => {
    //     // console.log(response)
    //     if (response.status == "success") {
    //         Toast.show('Video Downloaded successfully. Please Check in your Downloads Section.')
    //     }
    //     else if (response.status == "already") {
    //         Toast.show('File saved');
    //     } else if (!pauseDownloadRef?.current) {
    //         Toast.show('Something Went Wrong. Please Try Again Later')
    //     }
    //     setState({ savingItem: false });
    // }

 
    // componentDidUpdate = (prevProps, prevState) => {
    //     if (prevProps.progress != props.progress) {
    //         setState({ downloadProgress: props.progress })
    //     }
    // };
    
  

    

 
 

    const mountVideoPlayer = () => {
        console.log("mountVideoPlayer", instituteDetails)
        if (mode == "offline") {

            // navigation.navigate("videoplayer", { url: item?.fileAddress, title: item?.name, postingTime: item?.date, item: item })
            dispatch({type:SET_VIDEO_PLAYER_DATA,payload: {videoData:{
                url: item?.fileAddress, 
                title: item?.name, 
                postingTime: item?.date, 
                thumbnail:item?.thumbnail,
                source: item,
                instituteDetails:{id:instituteDetails.id,name:instituteDetails.name,logo:instituteDetails.logo}
            }}})
            dispatch({type:TOGGLE_VIDEO_PLAYER_MOUNT_STATE,payload:{mountVideoPlayer:true}})
        }
        else {
            if (videoType && videoType == "live") {
                // if(item?.streaming&&item?.videoLocation)
                if (item?.videoLocation) {
                    // navigation.navigate("videoplayer", { url: item?.videoLocation, title: item?.name, postingTime: item?.date, item: item })
                    dispatch({type:SET_VIDEO_PLAYER_DATA,payload:{videoData:{
                        url: item?.videoLocation,
                        title: item?.name,
                        postingTime: item?.date,
                        thumbnail:fileBaseUrl+item?.thumbnail,
                        source: item,
                        instituteDetails:{id:instituteDetails.id,name:instituteDetails.name,logo:instituteDetails.logo}
                    }}})
                    dispatch({type:TOGGLE_VIDEO_PLAYER_MOUNT_STATE,payload:{mountVideoPlayer:true}})
                } else {
                    Toast.show("Stream is not available")
                }
            } else {
                // navigation.navigate("videoplayer", { url: instituteServerBaseUrl + item?.videoLocation, title: item?.name, postingTime: item?.date, item: item })
                dispatch({type:SET_VIDEO_PLAYER_DATA,payload:{videoData:{
                    url: serverBaseUrl + item?.videoLocation,
                    title: item?.name,
                    postingTime: item?.date,
                    thumbnail: imageProvider(item?.videoThumb),
                    id:item?.id,
                    source: item,
                    instituteDetails:{id:instituteDetails.id,name:instituteDetails.name,logo:instituteDetails.logo}
                }}})
                dispatch({type:TOGGLE_VIDEO_PLAYER_MOUNT_STATE,payload:{mountVideoPlayer:true}})
            }
        }
    }
    const  Bull=({ isVisible })=> {
        return (
            <DarkColoredText style={{ fontSize: 40, color: theme.redColor, marginRight: 1 }}>
                {isVisible ? "•" : " "}
            </DarkColoredText>
        );
    }
    
        
        return (
            <TouchableWithoutFeedback
                onPress={() => {
                    // console.log(item)
                    mode == "student" ? (studentEnrolled || item?.demo ? (
                        <> 
                            {mountVideoPlayer()}
                            {addToHistory("video", item?.id)}
                        </>
                    ) : ( 
                        openPurchaseCourseModal ? openPurchaseCourseModal() : null
                    )) : (mountVideoPlayer())
                }
                } 
            >
                <View>
                    <View style={[styles.videoContainer,isBeingPlayed?{backgroundColor:theme.secondaryColor+"18",borderRadius:5}:{}]}>
                        <View>
                            <Image source={{uri:imageProvider(item?.videoThumb)}} style={styles.videoImage} />
                            {mode != "offline" && !studentEnrolled && !item?.demo ? (
                                <View style={{ position: 'absolute',justifyContent:"center",alignItems:"center", height: 30, width: 30, backgroundColor: theme.secondaryColor, borderRadius: 15, right: 5, top: 5 }}>
                                    {/* <Lock height={20} width={20} /> */}
                                    <Entypo name="lock" size={20} color={theme.primaryColor} />
                                </View>
                            ) : (null)} 
                        </View>
                        <View style={styles.videoColumn}>
                            <View style={{width:"100%"}}>
                                <DarkColoredText numberOfLines={1} style={styles.videoText}>{item?.name}</DarkColoredText>
                            </View>
                            <View>
                                {/* <DarkColoredText numberOfLines={2} style={styles.videoText}>{item?.description}</DarkColoredText> */}
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                <DarkColoredText size="12px">{numFormatter(item?.views)} views • {videoType == "live" ? (moment(item?.liveClassDate).format("d-M-Y") + " " + item?.liveClassTime) : (moment(item?.date).fromNow())}</DarkColoredText>
                                {/* {downloadMode && item?.videoType != "live" ? (
                                    <View style={{ flexDirection: 'column', marginRight: 10 }}>
                                        {state.savingItem ? (
                                            <TouchableWithoutFeedback
                                                onPress={cancelDownload}
                                            >
                                                <View style={{ width: 30, height: 30, marginBottom: 8, alignItems: 'center' }}>
                                                    <CircularProgress
                                                        value={state.downloadProgress}
                                                        radius={17}
                                                        inActiveStrokeColor={theme.greyColor}
                                                        inActiveStrokeOpacity={0.2}
                                                        inActiveStrokeWidth={5}
                                                        activeStrokeWidth={5}
                                                        textColor={'#000'}
                                                        valueSuffix={'%'}
                                                    />
                                                </View>
                                            </TouchableWithoutFeedback>

                                        ) : (
                                            state.downloadProgress == 100 ? (
                                                <MaterialIcons name="check-circle" size={25} />

                                            ) : (

                                                (studentEnrolled || item?.demo) ? <TouchableOpacity onPress={() => download(item, 'video')} style={{ marginBottom: 8 }}>
                                                    <View >
                                                        <Arrow_down_circle_black />
                                                    </View>
                                                </TouchableOpacity> : null


                                            )
                                        )}
                                    </View>
                                ) : (null)} */}
                            </View>
                            <View style={{ height: 10 }}>
                                {item?.videoType == "live" ? (
                                    videoType && videoType == "live" && item?.streaming ? (
                                        <View style={{ marginRight: 10 }}>
                                            <BlinkView timeout={1000}>
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Bull isVisible />
                                                    <DarkColoredText>Live</DarkColoredText>
                                                </View>
                                            </BlinkView>
                                        </View>
                                    ) : (null)
                                ) : (null)}
                            </View>
                        </View> 
                    </View>

                </View>
            </TouchableWithoutFeedback>
        )
    
}

const styles = StyleSheet.create({
    videoContainer:
    {
        marginTop: 10,
        flex: 1,
        flexDirection: 'row',

    },
    videoImage:
    {
        height: 100,
        width: 130,
        borderRadius: 10,  
    },
    videoColumn:
    {
        marginTop: 6,
        marginLeft: 5,
        flexDirection: 'column',
        flexWrap: 'wrap',
        width: (width - 140)
    },
    videoText:
    {
        marginBottom: 5,
        flexWrap: 'wrap',
        
        width: (width - 150)
    },


})

export default RenderVideo;
