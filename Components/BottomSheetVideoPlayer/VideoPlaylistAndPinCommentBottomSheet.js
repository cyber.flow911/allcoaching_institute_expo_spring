import { View, Text, ScrollView, FlatList, Dimensions, TouchableWithoutFeedback } from 'react-native'
import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react'   
import { BoldDarkColoredText, DarkColoredText, RowLayout, SectionContent, SectionView, SmallText } from '../GlobalStyle';
import { RoundInstitueLogo, VideoPlaylistAndPinCommentContainer } from './Style';
import PinnedAddComment from './Comments/PinnedAddComment';
import { appLogo, imageProvider } from '../config';
import { useSelector } from 'react-redux';
import moment from 'moment';
import VideoPlayerCourseVideosView from './VideoPlayerCourseVideosView/VideoPlayerCourseVideosView';
// import VideoPlayerCourseVideosView from '../InstituteCourseVideos/VideoPlayerCourseVideosView';


const height = Dimensions.get("window").height;
export const VIDEO_COMMENT_SHEET_MODES = {
    COMMENTS: "COMMENTS",
    DESCRIPTION: "DESCRIPTION",
}           

const VideoPlaylistAndPinCommentBottomSheet = forwardRef(({  videoPlayerHeight,  commentBottomSheetRef,}, ref) => {
    const videoData = useSelector(state => state.videoPlayer.videoData)
    return (
        <VideoPlaylistAndPinCommentContainer
            top={5}
            height={height - (videoPlayerHeight+20)}>
            <SectionView marginBottom="0px" marginTop="0px">
                <SectionContent marginTop="0px" marginBottom="0px"> 
                    <RowLayout>
                        <DarkColoredText  fontFamily="Raleway_600SemiBold" size="17px" numberOfLines={2}>
                            {videoData.title}
                        </DarkColoredText>
                    </RowLayout>
                    <RowLayout style={{marginVertical:3}}>
                        <SmallText fontSize="11px" style={{marginRight:10}}>{videoData.source.views} Views</SmallText>
                        <SmallText fontSize="11px">{moment(videoData.source?.date).fromNow()}</SmallText>
                    </RowLayout>
                    <RowLayout style={{alignItems:"center"}}>
                        <RoundInstitueLogo
                            source={{uri:imageProvider(videoData.instituteDetails?.logo)}}
                        />
                        <DarkColoredText fontFamily="Raleway_600SemiBold" size="13px">{videoData.instituteDetails?.name}</DarkColoredText>
                    </RowLayout>
                </SectionContent>
            </SectionView>
            <SectionView marginBottom="0px">
                <SectionContent>
                    {/* <SinglePinnedComment
                        onPress={() => {
                            commentBottomSheetRef.current?.commentInputFocused(false)
                            commentBottomSheetRef.current?.setHeightFactor(1)
                            commentBottomSheetRef.current?.setMode(VIDEO_COMMENT_SHEET_MODES.COMMENTS)
                            setTimeout(() => { 
                                commentBottomSheetRef.current?.sheet.show()
                            }, 100)
                        }}
                    /> */}
                    <PinnedAddComment
                        onPress={() => {
                            commentBottomSheetRef.current?.commentInputFocused(true)
                            commentBottomSheetRef.current?.setHeightFactor(1)
                            commentBottomSheetRef.current?.setMode(VIDEO_COMMENT_SHEET_MODES.COMMENTS)
                            setTimeout(() => {
                                commentBottomSheetRef.current?.show() 
                            }, 100)
                        }}
                    />
                </SectionContent>
            </SectionView>
            <SectionView marginTop="0px">
                <SectionContent>
                    <VideoPlayerCourseVideosView renderExtraFooterSpace />
                </SectionContent>
            </SectionView>
            <SectionView>
                <SectionContent>
                    <View style={{ height: 100 }} />
                </SectionContent>
            </SectionView>
        </VideoPlaylistAndPinCommentContainer>
    )
})

export default VideoPlaylistAndPinCommentBottomSheet