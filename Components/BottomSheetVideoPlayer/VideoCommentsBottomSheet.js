import { View, Text, ScrollView, FlatList, Dimensions, TouchableWithoutFeedback, Animated, PanResponder } from 'react-native'
import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react'
import { theme } from '../config';
import Comments from './Comments/Comments';
import { useSelector } from 'react-redux';
import { RowLayout } from '../GlobalStyle';
import { DraggableContainer, DraggableIcon } from './Style';


const height = Dimensions.get("window").height;
export const VIDEO_COMMENT_SHEET_MODES = {
    COMMENTS: "COMMENTS",
    DESCRIPTION: "DESCRIPTION",
}
const VideoCommentsBottomSheet = forwardRef(({  videoPlayerHeight }, ref) => {
    const videoData = useSelector(state => state.videoPlayer.videoData)
    const bottomSheetRef = useRef()
    const [mode, setMode] = useState()
    const sheetHeightBase = height - (videoPlayerHeight + 30)
    const [sheetHeight, setSheetHeight] = useState(sheetHeightBase)
    const [autoFocus, setAutoFocus] = useState(false)
    const [isVisible, setIsVisible] = useState(false)
    const [animatedHeight, setAnimatedHeight] = useState(new Animated.Value(0));
    const pan = useState(new Animated.ValueXY())[0]
    useEffect(() => {
        if (isVisible) {
            Animated.timing(animatedHeight, {
                toValue: sheetHeight,
                duration: 350,
                useNativeDriver: false
            }).start();
        } else {
            Animated.timing(animatedHeight, {
                toValue: 0,
                duration: 350,
                useNativeDriver: false
            }).start(() => {
                pan.setValue({ x: 0, y: 0 });
                setAnimatedHeight(new Animated.Value(0))
            });
        }
    }, [isVisible])
    useImperativeHandle(ref, () => {
        return {
            setMode: (mode) => {
                setMode(mode)
            },
            setHeightFactor: (heightFactor) => {
                setSheetHeight(sheetHeightBase * heightFactor)
            },
            sheet: bottomSheetRef.current,
            commentInputFocused: (value) => {
                setAutoFocus(value)
            },
            show: () => {
                setIsVisible(true)
            },
            close: () => {
                setIsVisible(false)
            }
        }
    })

    const closeSettingSheet = () => {
        bottomSheetRef.current.close()
    }
    const renderSheetContent = () => { 
        switch (mode) {
            case VIDEO_COMMENT_SHEET_MODES.COMMENTS:
                return <Comments
                    panHandlers={panResponder.panHandlers}
                    videoId={videoData?.source?.id}
                    unshiftCommets={() => { }}
                    autoFocus={autoFocus}
                    setIsSideScreenVisible={() => { }}
                    setIsCommentsVisible={() => { }}
                    width={'100%'}
                    height={'100%'}
                    flatlistHeight={'100%'}
                    closeComments={() => { setIsVisible(false) }}
                />
            default:
                return <View />
        }
    }


    const changeSheetMode = (mode, heightFactor) => {

        bottomSheetRef.current.close()
        setMode(mode)
        if (heightFactor) {
            setSheetHeight(sheetHeightBase * heightFactor)
        }
        setTimeout(() => {
            bottomSheetRef.current.show()
        }, 405)
    }


    const onPanResponderMove = (e, gestureState) => {
        if (gestureState.dy > 0) {
            Animated.event([null, { dy: pan.y }], {
                useNativeDriver: false,
            })(e, gestureState);
        }

    };

    const onPanResponderRelease = (e, gestureState) => {
        const gestureLimitArea = sheetHeight / 3;
        const gestureDistance = gestureState.dy;
        if (gestureDistance > gestureLimitArea) {
            setIsVisible(false);
        } else {
            Animated.spring(pan, { toValue: { x: 0, y: 0 }, useNativeDriver: false, }).start();
        }
    };

    const panResponder = useRef(PanResponder.create({
        onStartShouldSetPanResponder: (e, g) => true,
        // onMoveShouldSetPanResponder: (e, g) => {
        //     // console.log("onMoveShouldSetPanResponder",evt)
        //     // console.log(e.locationX, e.locationY, e.pageX, e.pageY);
        //     return  (Math.abs(g.dx) >= 100 || Math.abs(g.dy) >= 100)
        //     // return false
        // },
        onPanResponderMove,
        onPanResponderRelease,
    })).current

    const panStyle = {
        transform: pan.getTranslateTransform(),
    };

    return (
        // <BottomSheet
        //     hasDraggableIcon
        //     ref={bottomSheetRef}
        //     height={sheetHeight}
        //     draggable={false}
        //     modalStyle={{ justifyContent: "flex-end", height: sheetHeight, alignSelf: "flex-end", overflow: "hidden", backgroundColor: "red" }}
        //     containerStyle={{ position: "absolute", width: "100%", bottom: 0, justifyContent: "flex-end", alignSelf: "flex-end" }}
        //     backgroundStyle={{ height: sheetHeight }}
        //     backgroundColor={theme.guestColors.guestColor15}
        //     contentContainerStyle={{ width: "100%", alignSelf: "center", borderRadius: 10 }}
        //     onRequestClose={() => bottomSheetRef.current.close()}
        //     openFunction={openFunction}
        //     closeFunction={closeFunction}
        //     removeBackDropSupport={true}
        //     backgroundPointerEvents="none"
        //     radius={1}
        //     sheetBackgroundColor={theme.primaryColor}
        // >
        <Animated.View

            style={[
                panStyle,
                {
                    zIndex: 1000000000,
                    height: animatedHeight,
                    backgroundColor: theme.primaryColor
                }
            ]}
        >
            <RowLayout
                {...panResponder.panHandlers}
            >
                <DraggableContainer>
                    <DraggableIcon />
                </DraggableContainer>
            </RowLayout>
            {renderSheetContent()}
        </Animated.View>

        // </BottomSheet>
    )
})

export default VideoCommentsBottomSheet