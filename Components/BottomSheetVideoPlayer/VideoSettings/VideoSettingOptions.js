import { View, Text, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'
import React from 'react'
import { DarkColoredText, RowLayout } from '../../GlobalStyle';

import { CONSTANTS } from '../../config';
import { SHEET_MODES } from './VideoSettingsBottomSheet';
import Toast from 'react-native-simple-toast';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { theme } from '../../config';

const Bull = () => {
    return (
        <Text style={{ fontSize: 15, color: theme.labelOrInactiveColor, marginRight: 1 }}>
            {"•"}
        </Text>
    );
}
const VideoSettingOptions = ({ changeSheetMode, videoRate, videoQuality,setLoopCurrentVideo,loopCurrentVideo,closeSettingSheet }) => {

    const RenderOptionItem = ({ icon, label, currentValue, onPress, isActive }) => {
        return (
            <TouchableWithoutFeedback
                onPress={onPress}

            >
                <RowLayout style={{ marginHorizontal: 20, marginBottom: 15 }}>
                    <View style={{ marginRight: 10 }}>
                        {icon}
                    </View>
                    <RowLayout>
                        <DarkColoredText size={'16px'} style={{ marginLeft: 10, marginRight: 5 }}>{label}</DarkColoredText>
                        {currentValue ? (<Bull />) : null}
                        <DarkColoredText color={theme.silverColor} style={{ marginLeft: 5 }}>{currentValue}</DarkColoredText>
                    </RowLayout>
                </RowLayout>
            </TouchableWithoutFeedback>
        )
    }
    return (
        <View>
            <RenderOptionItem
                icon={<MaterialIcons name="settings" size={CONSTANTS.NORMAL_ICON_SIZE - 15} />}
                label={"Quality"}
                currentValue={videoQuality + "p"}
                onPress={() => { 
                    changeSheetMode(SHEET_MODES.QUALITY, 1.5)  
                }}
            />
            <RenderOptionItem
                icon={<MaterialCommunityIcons name="play-speed" size={CONSTANTS.NORMAL_ICON_SIZE - 15} />}
                label={"Playback speed"}
                currentValue={videoRate == 1 ? "Normal" : videoRate + "x"}
                onPress={() => { changeSheetMode(SHEET_MODES.SPEED, 2.2) }}
            />
            <RenderOptionItem
                icon={<MaterialIcons name="loop" size={CONSTANTS.NORMAL_ICON_SIZE - 15} />}
                label={"Loop Video"}
                currentValue={loopCurrentVideo?"On":"Off"}
                onPress={() => { 
                        setLoopCurrentVideo(!loopCurrentVideo);
                        closeSettingSheet() 
                        setTimeout(() => {
                            Toast.show("Loop Video is "+(!loopCurrentVideo?"On":"Off"), Toast.SHORT);
                        },405)
                }}
            />
            {/* <RenderOptionItem
                icon={<MaterialIcons name="help-outline" size={CONSTANTS.NORMAL_ICON_SIZE - 15} />}
                label={"Help and Feedback"}
                currentValue={""}
                onPress={() => { }}
            /> */}
        </View>
    )
}

export default VideoSettingOptions