import styled from 'styled-components/native'
import { Dimensions } from 'react-native';
import { theme } from '../../config';
 

export const SheetContainer = styled.View`
    flex:1;
    marginTop: 10px;
    backgroundColor: ${theme.primaryColor};
`