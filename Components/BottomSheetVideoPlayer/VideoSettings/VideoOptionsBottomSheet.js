import { View, Text,ScrollView, FlatList, Dimensions, TouchableWithoutFeedback } from 'react-native'
import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react'
import BottomSheet from "react-native-gesture-bottom-sheet";
import VideoSettingOptions from './VideoSettingOptions';
import { SheetContainer } from './Style';
import VideoSpeedOptions from './VideoSpeedOptions';
import VideoQualityOptions from './VideoQualityOptions';
import { theme } from '../../config';
import {useDeviceOrientation} from '@react-native-community/hooks'


const height = Dimensions.get("window").height;
const width = Dimensions.get('window').width;
     
export const SHEET_MODES = {
    SPEED: "SPEED",
    QUALITY: "QUALITY",
    COMMENTS: "COMMENTS",
    DESCRIPTION: "DESCRIPTION", 
}
const VideoOptionsBottomSheet = forwardRef(({openFunction,closeFunction,videoQuality,setVideoQuality, setVideoRate, videoRate,setLoopCurrentVideo,loopCurrentVideo }, ref) => {

    const bottomSheetRef = useRef()
    const [mode, setMode] = useState()
    const orientation = useDeviceOrientation()
    console.log('orientation is:', orientation)
    const sheetHeightBase = orientation!="landscape"?(height / 3.5):(width / 2.5)
    const [sheetHeight, setSheetHeight] = useState(sheetHeightBase)

    useEffect(() => {
        setSheetHeight(sheetHeightBase)
    },[orientation])

    useImperativeHandle(ref, () => {
        return {
            setMode: (mode) => {
                setMode(mode)
            },
            setHeightFactor: (heightFactor) => {
                setSheetHeight(sheetHeightBase * heightFactor)
            },
            sheet: bottomSheetRef.current
        }
    })

    const closeSettingSheet = () => {
        bottomSheetRef.current.close()
    }
    const renderSheetContent = () => {
        switch (mode) {
            case SHEET_MODES.SPEED:
                return <VideoSpeedOptions
                    closeSettingSheet={closeSettingSheet}
                    videoRate={videoRate}
                    changeVideoSpeed={setVideoRate}
                />
            case SHEET_MODES.QUALITY:
                return <VideoQualityOptions
                    setVideoQuality={setVideoQuality}
                    videoQuality={videoQuality}
                    closeSettingSheet={closeSettingSheet}
                />
            // case SHEET_MODES.COMMENTS:
            //     return <CommentsOptions/>
            // case SHEET_MODES.DESCRIPTION:
            //     return <DescriptionOptions/> 
            default:
                return <View />
        }
    }

 
    return (
        <BottomSheet
            hasDraggableIcon
            ref={bottomSheetRef}
            height={sheetHeight}
            backgroundColor={theme.secondaryColor + '6A'}
            sheetBackgroundColor={theme.primaryColor}
            contentContainerStyle={{ width: "95%", marginBottom: 20, alignSelf: "center", borderRadius: 10 }}
            onRequestClose={() => bottomSheetRef.current.close()}
            openFunction={openFunction}
            closeFunction={closeFunction}
            dragIconStyle={{ height:4 }}
            
        >
            <ScrollView
                showsVerticalScrollIndicator={false}
            >
                <SheetContainer>
                    {renderSheetContent()}
                </SheetContainer>
            </ScrollView>
        </BottomSheet>
    )
})

export default VideoOptionsBottomSheet