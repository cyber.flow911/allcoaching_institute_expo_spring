import { View, Text, TouchableWithoutFeedback } from 'react-native'
import React from 'react'
import { DarkColoredText, Hr, RowLayout } from '../../GlobalStyle';
import { CONSTANTS,theme } from '../../config';
 
import { MaterialIcons } from '@expo/vector-icons';

const VideoQualityOptions = ({videoQuality,setVideoQuality,closeSettingSheet}) => {
    const activeVideoQuality = videoQuality;        
    const renderOptionItem = (label,  value) => {
        return (
            <TouchableWithoutFeedback
                onPress={()=>{
                    setVideoQuality(value)
                    closeSettingSheet()
                }}
            >
                <RowLayout style={{marginHorizontal:20,marginBottom:15}}>
                    <View style={{marginRight:15}}>
                        {activeVideoQuality == value ? <MaterialIcons name="check" size={CONSTANTS.NORMAL_ICON_SIZE-5} color={theme.btn_dark_background_color} /> : <View style={{height:CONSTANTS.NORMAL_ICON_SIZE-5,width:CONSTANTS.NORMAL_ICON_SIZE-5}}/>}
                    </View>
                    <RowLayout> 
                        <DarkColoredText fontFamily="Raleway_500Medium" size={'16px'} style={{ marginLeft: 10,marginRight:5 }}>{label}</DarkColoredText> 
                    </RowLayout>
                </RowLayout>
            </TouchableWithoutFeedback>
        )
    }
    const renderCancel = () => {
        return(
            <TouchableWithoutFeedback
                onPress={()=>{
                    closeSettingSheet()
                }}
            >
                <RowLayout style={{marginHorizontal:20,marginBottom:15}}>
                    <View style={{marginRight:15}}>
                        <MaterialIcons name="close" size={CONSTANTS.NORMAL_ICON_SIZE-5} color={theme.labelOrInactiveColor} /> 
                    </View>
                    <RowLayout> 
                        <DarkColoredText fontFamily="Raleway_500Medium" size={'16px'} style={{ marginLeft: 10,marginRight:5 }}>Cancel</DarkColoredText> 
                    </RowLayout>
                </RowLayout>
            </TouchableWithoutFeedback>
        )
    }
    return (
    <View>
        {renderOptionItem("720p",720)} 
        {renderOptionItem("480p",480)} 
        {renderOptionItem("360p",360)}  
        {renderOptionItem("240p",240)} 
        {renderOptionItem("144p",144)}  
        <Hr height={"1.5px"} style={{marginBottom:10}} color={theme.labelOrInactiveColor}/>
        {renderCancel()}
        
    </View>
    )
}

export default VideoQualityOptions