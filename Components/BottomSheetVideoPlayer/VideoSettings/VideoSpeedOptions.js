import { View, Text, TouchableWithoutFeedback } from 'react-native'
import React from 'react'
import { DarkColoredText, Hr, RowLayout } from '../../GlobalStyle';
import { CONSTANTS, theme } from '../../config';
import { MaterialIcons } from '@expo/vector-icons';

const VideoSpeedOptions = ({changeVideoSpeed,videoRate,closeSettingSheet}) => {
    const activeVideoSpeed = videoRate;        
    const renderOptionItem = (label,  value) => {
        return (
            <TouchableWithoutFeedback
                onPress={()=>{
                    changeVideoSpeed(value)
                    closeSettingSheet()
                }}
            >
                <RowLayout style={{marginHorizontal:20,marginBottom:15}}>
                    <View style={{marginRight:15}}>
                        {activeVideoSpeed == value ? <MaterialIcons name="check" size={CONSTANTS.NORMAL_ICON_SIZE-5} color={theme.labelOrInactiveColor} /> : <View style={{height:CONSTANTS.NORMAL_ICON_SIZE-5,width:CONSTANTS.NORMAL_ICON_SIZE-5}}/>}
                    </View>
                    <RowLayout> 
                        <DarkColoredText fontFamily="Raleway_500Medium" size={'16px'} style={{ marginLeft: 10,marginRight:5 }}>{label}</DarkColoredText> 
                    </RowLayout>
                </RowLayout>
            </TouchableWithoutFeedback>
        )
    }
    const renderCancel = () => {
        return(
            <TouchableWithoutFeedback
                onPress={()=>{
                    closeSettingSheet()
                }}
            >
                <RowLayout style={{marginHorizontal:20,marginBottom:15}}>
                    <View style={{marginRight:15}}>
                        <MaterialIcons name="close" size={CONSTANTS.NORMAL_ICON_SIZE-5} color={theme.labelOrInactiveColor} /> 
                    </View>
                    <RowLayout> 
                        <DarkColoredText fontFamily="Raleway_500Medium" size={'16px'} style={{ marginLeft: 10,marginRight:5 }}>Cancel</DarkColoredText> 
                    </RowLayout>
                </RowLayout>
            </TouchableWithoutFeedback>
        )
    }
    return (
    <View>
        {renderOptionItem("0.25x",0.25)} 
        {renderOptionItem("0.5x",0.5)} 
        {renderOptionItem("0.75x",0.75)} 
        {renderOptionItem("Normal",1)} 
        {renderOptionItem("1.25x",1.25)} 
        {renderOptionItem("1.5x",1.5)} 
        {renderOptionItem("1.75x",1.75)} 
        {renderOptionItem("2x",2)} 
        <Hr height={"2px"} style={{marginBottom:10}} color={theme.labelOrInactiveColor}/>
        {renderCancel()}
    </View>
    )
}

export default VideoSpeedOptions