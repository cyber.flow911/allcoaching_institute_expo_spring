import { View, Text, ScrollView, FlatList, Dimensions, TouchableWithoutFeedback } from 'react-native'
import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react'
import BottomSheet from "react-native-gesture-bottom-sheet";
import VideoSettingOptions from './VideoSettingOptions';
import { SheetContainer } from './Style';
import VideoSpeedOptions from './VideoSpeedOptions';
import VideoQualityOptions from './VideoQualityOptions';
import { theme } from '../../config';
import VideoOptionsBottomSheet from './VideoOptionsBottomSheet';


const height = Dimensions.get("window").height;
export const SHEET_MODES = {
    SPEED: "SPEED",
    QUALITY: "QUALITY",
    COMMENTS: "COMMENTS",
    DESCRIPTION: "DESCRIPTION",
    SETTINGS: "SETTINGS",
}
const VideoPlayerBottomSheet = forwardRef(({ openFunction, closeFunction,playerRef }, ref) => {

    const bottomSheetRef = useRef()
    const [mode, setMode] = useState()
    const sheetHeightBase = height / 3.5
    const [sheetHeight, setSheetHeight] = useState(sheetHeightBase)
    const optionSheetRef = useRef()
    const [videoQuality, setVideoQuality] = useState(720)
    const [videoRate, setVideoRate] = useState(1)
    const [loopCurrentVideo, setLoopCurrentVideo] = useState(false)

    useEffect(() => {
        playerRef.current?.setVideoQuality(videoQuality)
    },[videoQuality])
    useEffect(() => {
        playerRef.current?.setVideoRate(videoRate)
    },[videoRate])
    useEffect(() => {
        playerRef.current?.setLoopCurrentVideo(loopCurrentVideo)
    },[loopCurrentVideo])

    useImperativeHandle(ref, () => {
        return {
            setMode: (mode) => {
                setMode(mode)
            },
            setHeightFactor: (heightFactor) => {
                setSheetHeight(sheetHeightBase * heightFactor)
            },
            sheet: bottomSheetRef.current
        }
    })

    const closeSettingSheet = () => {
        bottomSheetRef.current.close()
    }
    const renderSheetContent = () => {
        switch (mode) {
            case SHEET_MODES.SPEED:
                return <VideoSpeedOptions
                    closeSettingSheet={closeSettingSheet}
                    videoRate={videoRate}
                    changeVideoSpeed={setVideoRate}
                />
            case SHEET_MODES.QUALITY:
                return <VideoQualityOptions
                    setVideoQuality={setVideoQuality}
                    videoQuality={videoQuality}
                    closeSettingSheet={closeSettingSheet}
                />
            // case SHEET_MODES.COMMENTS:
            //     return <CommentsOptions/>
            // case SHEET_MODES.DESCRIPTION:
            //     return <DescriptionOptions/>

            default:
                return <View />
        }
    }


    const changeSheetMode = (mode, heightFactor) => {
        console.log("called, mode: ", mode, " heightFactor: ", heightFactor)
        setMode(mode)
        if (heightFactor) {
            setSheetHeight(sheetHeightBase * heightFactor)
        }
        optionSheetRef.current.setHeightFactor(heightFactor)
        optionSheetRef.current.setMode(mode)
        setTimeout(() => {
            optionSheetRef.current.sheet.show()
        }, 100)

        bottomSheetRef.current.close()
    }
    return (
        <>
            <BottomSheet
                hasDraggableIcon
                ref={bottomSheetRef}
                height={sheetHeight}
                backgroundColor={theme.secondaryColor + '6A'}
                sheetBackgroundColor={theme.primaryColor}
                contentContainerStyle={{ width: "95%", marginBottom: 20, alignSelf: "center", borderRadius: 10 }}
                onRequestClose={() => bottomSheetRef.current.close()}
                openFunction={openFunction}
                closeFunction={closeFunction}
                dragIconStyle={{ height: 4 }}

            >
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <SheetContainer>
                        {/* {renderSheetContent()} */}
                        <VideoSettingOptions
                            sheetRef={bottomSheetRef}
                            optionSheetRef={optionSheetRef}
                            videoRate={videoRate}
                            changeSheetMode={changeSheetMode}
                            videoQuality={videoQuality}
                            setLoopCurrentVideo={setLoopCurrentVideo}
                            loopCurrentVideo={loopCurrentVideo}
                            closeSettingSheet={closeSettingSheet}
                        />
                    </SheetContainer>
                </ScrollView>
            </BottomSheet>
            <VideoOptionsBottomSheet
                ref={optionSheetRef}
                videoQuality={videoQuality}
                setVideoQuality={setVideoQuality}
                setVideoRate={setVideoRate}
                videoRate={videoRate}
                setLoopCurrentVideo={setLoopCurrentVideo}
                loopCurrentVideo={loopCurrentVideo}
            />
        </>
    )
})

export default VideoPlayerBottomSheet