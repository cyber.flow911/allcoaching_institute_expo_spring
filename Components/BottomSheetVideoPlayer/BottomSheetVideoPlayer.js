import { View, Text, Dimensions, Animated, StatusBar, PanResponder, StyleSheet, LayoutAnimation, TouchableWithoutFeedback, ScrollView } from 'react-native'
import React, { forwardRef, useEffect, useImperativeHandle, useRef, useState } from 'react'


import VideoPlayer from './VideoPlayer';
import { serverBaseUrl, theme } from '../config';
import { SheetContent, VideoPlayerContainer } from './Style';
import { useDispatch, useSelector } from 'react-redux';
import { CLEAR_VIDEO_PLAYER_DATA, SET_VIDEO_PLAYER_DATA, TOGGLE_VIDEO_PLAYER_MOUNT_STATE, TOGGLE_VIDEO_PLAYER_OPEN_IN_MINIMIZED_STATE } from '../Actions/types';
import useStateRef from 'react-usestateref';
import VideoCommentsBottomSheet, { VIDEO_COMMENT_SHEET_MODES } from './VideoCommentsBottomSheet';
import VideoPlaylistAndPinCommentBottomSheet from './VideoPlaylistAndPinCommentBottomSheet';
import { CONSTANTS, fileBaseUrl } from '../config';
import { imageProvider } from '../config';
import VideoPlayerBottomSheet from './VideoSettings/VideoSettingsBottomSheet';
const DEVICE_WIDTH = Dimensions.get("window").width;
// const MARGIN_TOP = Platform.OS === "ios" ? 24 : StatusBar.currentHeight;

const DEVICE_WINDOW_HEIGHT = Dimensions.get("window").height;
const DEVICE_SCREEN_HEIGHT = Dimensions.get("screen").height;
const DEVICE_EXTRA_SAPCES = (DEVICE_SCREEN_HEIGHT - DEVICE_WINDOW_HEIGHT)

const MARGIN_TOP = DEVICE_EXTRA_SAPCES == 0 ? 24 : 0;
const DEFAULT_VIDEO_PLAYER_HEIGHT = DEVICE_WINDOW_HEIGHT * 0.26;

const BottomSheetVideoPlayer = forwardRef(({ }, ref) => {
    const dispatch = useDispatch();
    const videoPlayerRef = useRef();
    const videoData = useSelector(state => state.videoPlayer.videoData)

    const openInMinimizedState = useSelector(state => state.videoPlayer.openInMinimizedState)
    const isPanResponderActiveRef = useRef(true);
    const [extraMarginTop, setExtraMarginTop, extraMarginTopRef] = useStateRef(MARGIN_TOP);
    const MINIMISED_VIDEO_PLAYER_HEIGHT = 60;
    const MINIMISED_VIDEO_PLAYER_WIDTH = MINIMISED_VIDEO_PLAYER_HEIGHT * 1.65;
    const maxHeight = DEVICE_WINDOW_HEIGHT - extraMarginTop;
    const [collapsed, setCollapsed, collapsedRef] = useStateRef(true);
    const [orientation, setOrientation, orientationRef] = useStateRef("PORTRAIT");
    const [videoPlayerCurrentOrientationHeight, setVideoPlayerCurrentOrientationHeight, videoPlayerCurrentOrientationHeightRef] = useStateRef(DEFAULT_VIDEO_PLAYER_HEIGHT)
    const videoPlayerHeight = collapsed ? MINIMISED_VIDEO_PLAYER_HEIGHT : videoPlayerCurrentOrientationHeightRef.current
    const videoPlayerWidth = collapsed ? MINIMISED_VIDEO_PLAYER_WIDTH : DEVICE_WIDTH
    const commentBottomSheetRef = useRef()
    let animatedHeight = useRef(new Animated.Value(0)).current;
    let currentHeightRef = useRef(0);
    const activeCourseDetails = useSelector(state => state.institute.activeCourseDetails)
    const activeCourse = activeCourseDetails.id;
    const activePlaylist = useSelector(state => state.institute.videoActivePlaylist[activeCourse]) || -1;
    const DATA_KEY = activeCourse + "_" + activePlaylist
    const videos = useSelector(state => state.institute.courseVideos[DATA_KEY]);
    const instituteDetails = useSelector(state => state.institute.instituteDetails)
    const studentEnrolled = useSelector(state => state.institute.userEnrolledCourses[activeCourse]);
    const isTabBarMounted = useSelector(state => state.layout.isTabBarVisible);
    const purchaseBottomSheetRef = useRef(null);
    const videoSettingsBottomSheetRef = useRef(null); 
    const openPurchaseCourseModal = () => {
        purchaseBottomSheetRef.current?.show();
    }


    // useEffect(() => {
    //     StatusBar.setBarStyle("dark-content", true);
    // },[])
    useEffect(() => {
        if (collapsed) {
            showFull()
        } else {
            showMini()
        }
    }, [])

    const startNextVideo = () => {
        const item = getNextVideo();
        if (item) {
            if (studentEnrolled) {
                setVideoDataToPlay(item)
                return true
            } else if (item?.demo) {
                setVideoDataToPlay(item)
                return true
            } else {
                openPurchaseCourseModal()
                return false
            }
            
        }

        return false
    }


    const setVideoDataToPlay = (item) => {
        dispatch({
            type: SET_VIDEO_PLAYER_DATA, payload: {
                videoData: {
                    url: serverBaseUrl + item?.videoLocation,
                    title: item?.name,
                    postingTime: item?.date,
                    thumbnail: imageProvider(item?.videoThumb),
                    id: item?.id,
                    source: item,
                    instituteDetails: { id: instituteDetails.id, name: instituteDetails.name, logo: instituteDetails.logo }
                }
            }
        })
        dispatch({ type: TOGGLE_VIDEO_PLAYER_MOUNT_STATE, payload: { mountVideoPlayer: true } })
    }
    const getNextVideo = () => {
        const activeVideoIndex = videos?.findIndex(item => item.id == videoData?.id)
        if (activeVideoIndex != -1 && activeVideoIndex < videos.length - 1) {
            return videos[activeVideoIndex + 1];
        } else {
            return false
        }
    }
    const onPanResponderMove = (event, gestureState) => {
        if (isPanResponderActiveRef.current) {
            commentBottomSheetRef.current?.close()
        }
        if (!collapsedRef.current && gestureState.dy < 0) {
            return
        } else {
            Animated.timing(animatedHeight, {
                toValue: currentHeightRef.current - gestureState.dy,
                duration: 0,
                useNativeDriver: false
            }).start()
        }
    };

    const onPanResponderRelease = (event, gestureState) => {
        if (!collapsedRef.current && gestureState.dy < 0) {
            return
        }
        if (gestureState.dy < -100 || gestureState.dy < 50) {
            setCollapsed(false)
            if (orientationRef.current === "PORTRAIT") {
                showFull()
            } else {
                animateHeightOnOrientationChange(DEVICE_WIDTH)
            }
        } else {
            if (orientationRef.current !== "PORTRAIT") {
                videoPlayerRef.current.changeOrientationToPortait()
                showFull()

            } else {
                setCollapsed(true)
                showMini()
            }
        }
    };

    const setPanResponderActiveState = (state) => {
        isPanResponderActiveRef.current = state
    }
    const panResponder = React.useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: (e, g) => false,
            onMoveShouldSetPanResponder: (e, g) => {
                // return false  
                if (Math.abs(g.dx) >= 20 && Math.abs(g.dy) < Math.abs(g.dx)) {

                    return false
                }
                return isPanResponderActiveRef.current && (Math.abs(g.dx) >= 100 || Math.abs(g.dy) >= 100)
                // return false 
            },
            onPanResponderMove,
            onPanResponderRelease,
            // onPanResponderTerminationRequest: (e, g) => true,
        })
    ).current;
    const showFull = () => {
        Animated.timing(animatedHeight, {
            toValue: maxHeight,
            duration: 350,
            useNativeDriver: false
        }).start(() => {
            currentHeightRef.current = maxHeight
            setCollapsed(false)
        });
    };
    const animateHeightOnOrientationChange = (value) => {
        Animated.timing(animatedHeight, {
            toValue: value,
            duration: 350,
            useNativeDriver: false
        }).start(() => {
            currentHeightRef.current = value
        });
    }
    const showMini = () => {
        Animated.timing(animatedHeight, {
            toValue: 60,
            duration: 350,
            useNativeDriver: false
        }).start(() => {
            currentHeightRef.current = 60
            setCollapsed(true)
        });
    };


    useEffect(() => {
        if (videoData?.url) {
            if (!openInMinimizedState) {
                showFull()
            } else {
                dispatch({ type: TOGGLE_VIDEO_PLAYER_OPEN_IN_MINIMIZED_STATE, payload: { openInMinimizedState: false } })
                showMini()
            }
        }
    }, [videoData])



    const unMountVideoPlayer = () => {
        dispatch({ type: TOGGLE_VIDEO_PLAYER_MOUNT_STATE, payload: { mountVideoPlayer: false } })
        dispatch({ type: CLEAR_VIDEO_PLAYER_DATA })
    }

    // useEffect(() => {
    //     if (videoData?.id) {
    //         updateVideoView(videoData?.id)
    //     }
    // }, [videoData?.id])

    const onOrientationChange = (orientation) => {
        if (orientation === "LANDSCAPE") {
            currentHeightRef.current = DEVICE_WIDTH
            animateHeightOnOrientationChange(DEVICE_WIDTH)
        } else {
            showFull()
        }
        commentBottomSheetRef.current?.close()
        setVideoPlayerCurrentOrientationHeight(orientation == "LANDSCAPE" ? DEVICE_WINDOW_HEIGHT : DEFAULT_VIDEO_PLAYER_HEIGHT)
        setOrientation(orientation)
    }

    return (
        <>
            <Animated.View
                style={[
                    {
                        position: "absolute",
                        bottom: collapsed ? isTabBarMounted ? 55 : 0 : 0,
                        height: animatedHeight,
                        backgroundColor: theme.primaryColor,
                        zIndex: 1000000
                    }
                ]}
            >
                <StatusBar
                    animated={true}
                    backgroundColor={!collapsed ? '#000000' : "#fff"}
                    barStyle={!collapsed ? "light-content" : "dark-content"}

                />
                <SheetContent
                    pointerEvents={"box-none"}
                >
                    <VideoPlayerContainer
                        flexDirection={collapsed ? "row" : "column"}
                    >
                        <VideoPlayer
                            ref={videoPlayerRef}
                            style={{
                                width: videoPlayerWidth,
                                height: videoPlayerHeight,
                                alignSelf: collapsed ? "flex-start" : "center",
                            }}
                            source={{ uri:"https://vz-5ac6a105-de0.b-cdn.net/28acfa53-9095-49a8-9e0f-a1e711168c28/playlist.m3u8"||videoData?.url }}
                            poster={videoData?.thumbnail}
                            title={videoData?.title}
                            autoPlay={true}
                            playInBackground={false}
                            setPanResponderActiveState={setPanResponderActiveState}
                            onOrientationChange={onOrientationChange}
                            enableControls={!collapsed}
                            showHeader={true}
                            showSeeking10SecondsButton={true}
                            showCoverButton={false}
                            showFullScreenButton={true}
                            showSettingButton={true}
                            showMuteButton={false}
                            isCollapsed={collapsed}
                            unMountVideoPlayer={unMountVideoPlayer}
                            panHandlers={panResponder.panHandlers}
                            showFull={() => { setCollapsed(false); showFull() }}
                            showMini={() => { setCollapsed(true); showMini() }}
                            startNextVideo={startNextVideo}
                            videoSettingsBottomSheetRef={videoSettingsBottomSheetRef}
                        />
                    </VideoPlayerContainer>
                    {!collapsed && orientation === "PORTRAIT" ? (
                        <>

                            <VideoPlaylistAndPinCommentBottomSheet
                                videoPlayerHeight={DEFAULT_VIDEO_PLAYER_HEIGHT}
                                commentBottomSheetRef={commentBottomSheetRef}
                            />
                        </>
                    ) : (null)}
                </SheetContent>
            </Animated.View>
            <VideoCommentsBottomSheet
                ref={commentBottomSheetRef}
                videoPlayerHeight={DEFAULT_VIDEO_PLAYER_HEIGHT}
                openFunction={() => {
                    console.log("called")
                }}
                closeFunction={() => {
                    console.log("called")
                }}
            />
            <VideoPlayerBottomSheet
                ref={videoSettingsBottomSheetRef}
                playerRef={videoPlayerRef}
                openFunction={() => {
                    console.log("called")
                    setPanResponderActiveState(false)
                }}
                closeFunction={() => {
                    console.log("called")
                    setPanResponderActiveState(true)
                }}
            />

        </>
    )
})


export default BottomSheetVideoPlayer