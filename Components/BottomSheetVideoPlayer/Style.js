import styled from 'styled-components/native'
import { Dimensions } from 'react-native';
import { theme } from '../config';

export const SheetContainer = styled.View`
    position: absolute; 
    left: 0px;
    right: 0px;
    overflow: hidden; 
    bottom: 0px;
    height: ${props => props.height || '100%'};
    top: ${props => props.top || '0px'};
    background-color:${theme.secondaryColor};  
    margin-top: ${props => props.marginTop || '0px'};
    margin-bottom: ${props => props.marginBottom || '20px'};
    z-index: 1000;

`
export const VideoPlaylistAndPinCommentContainer = styled.View` 
    background-color:${theme.primaryColor};  
    margin-top: ${props => props.marginTop || '0px'};
    margin-bottom: ${props => props.marginBottom || '0px'};
    
`
export const SheetContent = styled.View`
        width:100%; 
        flexDirection: column; 
         
`

export const VideoPlayerContainer = styled.View`
    width: 100%;
    background-color:${theme.primaryColor};
    flex-direction: ${props => props.flexDirection || 'column'};    
    align-items: center;
    justify-content: center;
    align-self: center;
`

export const ButtonBackgroundView = styled.TouchableOpacity`
    
    background-color: ${theme.secondaryColor + "66"};  
    border-radius: 50px;
    padding:5px;
    
`


export const DraggableContainer = styled.View`
    width: 100%;
    align-items: center;
    background-color: transparent;
`

export const DraggableIcon = styled.View`
    width: 40px;
    height: 4px;
    borderRadius: 3px;
    margin: 10px;
    marginBottom: 0px ;
    margin-top: 0px;
    backgroundColor: #A3A3A3;
`
export const RoundInstitueLogo = styled.Image`
    width: 30px;
    height: 30px;
    borderRadius: 20px;  
    margin-right:5px;
    margin-top: 10px; 
        
`