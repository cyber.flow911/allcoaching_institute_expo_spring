import styled from 'styled-components/native'
import { Dimensions } from 'react-native'; 
import { theme } from '../../config'; 
import { RowLayout, WidthResponsiveRowLayout } from '../../GlobalStyle';
export const SinglePinnedCommentContainer = styled.View`
    width:100%;
    flexDirection: column;
    backgroundColor:${theme.btn_dark_background_color+"14"};
    padding:10px; 
    border-radius:15px; 
`

export const VideoCommentInputButton = styled.TouchableOpacity`
    width:85%;
    height: 25px;
    border-radius: 20px;
    background-color:${theme.greyColor+"14"};
    justify-content:center;
    padding-left:10px;
`


export const VideoCommentFilterButton = styled.TouchableOpacity`
    border-radius: 20px;
    background-color:${props=>props.active?theme.secondaryColor:theme.labelOrInactiveColor+"14"};
    justify-content:center;
    align-items:center;
    padding-left:12px;
    padding-right:12px;
    padding-top:5px;
    padding-bottom:5px;

`

export const CommunityGuidelinesNoticeContainer = styled(WidthResponsiveRowLayout)`
    width:100%; 
    backgroundColor:${theme.silverColor+"14"};
    padding:10px;
`


export const SheetContainer = styled.View`   
    background-color:${theme.primaryColor}; 
    height: ${props => props.height || '100%'};
    margin-top: ${props => props.marginTop || '0px'};
    margin-bottom: ${props => props.marginBottom || '0px'};
`
export const DarkTranslucentOverlay = styled.View`
    position: absolute;
    left: 0px;
    right: 0px;
    bottom: 0px;
    height: ${props => props.height || '100%'};
    top: ${props => props.top || '0px'};
    background-color:${theme.secondaryColor+"66"};
    margin-top: ${props => props.marginTop || '0px'};
    margin-bottom: ${props => props.marginBottom || '0px'};
    z-index: 1000;
`