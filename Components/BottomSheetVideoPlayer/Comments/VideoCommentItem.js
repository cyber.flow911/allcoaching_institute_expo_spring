import moment from 'moment';
import React, { useRef } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import { DarkColoredText, RowLayout, SmallText } from '../../GlobalStyle';
import { Assets, imageProvider } from '../../config';
import ImageViewExtension from '../../Utils/ImageViewExtension/ImageViewExtension';

const VideoCommentItem = (props) => {
    const item = props.item;
    const mode = props.mode;
    // const item={
    //     "comment": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    //     "commentTime": "2021-03-31T12:00:00.000Z",
    //     "student": {
    //         "name": "Student 1",
    //         "studentImage": "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__340.jpg"
    //     }
    // } 
    return (
        <View>
            <RowLayout style={{ padding: 10 }}>
                <ImageViewExtension
                    source={{uri:imageProvider(item?.student?.studentImage)}}
                    style={{ height: 30, width: 30, borderRadius: 30, marginRight: 10, alignSelf: 'flex-start' }}
                    errorImage={Assets.profile.profileIcon}
                />
                <View style={{ flex: 1, marginLeft: 10, marginTop: 2 }}>
                    <RowLayout>
                        <SmallText>{item?.student?.name}{' • '}</SmallText>
                        <SmallText>{moment(item?.commentTime).fromNow()}</SmallText>
                    </RowLayout>
                    <View style={{ flexShrink: 1 }}>
                        <DarkColoredText size="14px" style={{ flexWrap: 'wrap' }}>{item?.comment}</DarkColoredText>
                    </View>
                </View>
            </RowLayout>
        </View>
    )
}

export default VideoCommentItem;
