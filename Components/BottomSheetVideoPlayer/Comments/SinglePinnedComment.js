import { View, Text, TouchableWithoutFeedback } from 'react-native'
import React from 'react'
import { SinglePinnedCommentContainer } from './Style'
import { DarkColoredText, RowLayout, SmallText } from '../../../GlobalStyle'
import VideoCommentItem from './VideoCommentItem'
import FastImage from 'react-native-fast-image'
import ImageViewExtension from '../../ImageViewExtension/ImageViewExtension'
import { getPlatformFontName, ImageResolver } from '../../../Utils'
import { fontNames, Images } from '../../../Assets'
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons'
import { CONSTANTS } from '../../../config'
import { theme } from '../../../theme'
import MaterialCommunityIcons from 'react-native-vector-icons/dist/MaterialCommunityIcons'

const SinglePinnedComment = ({ onPress }) => {

    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <SinglePinnedCommentContainer>
                <RowLayout style={{ marginBottom: 10 }}>
                    <DarkColoredText size="16px" fontFamily={getPlatformFontName(fontNames.Urbanist_semibold)} style={{ marginRight: 10 }}>
                        Comments
                    </DarkColoredText>
                    <SmallText fontSize="14px">
                        14
                    </SmallText>
                </RowLayout>
                <RowLayout>
                    <ImageViewExtension
                        source={Images.userAvatar}
                        errorImage={Images.userAvatar}
                        style={{ height: 30, width: 30, borderRadius: 20, marginRight: 10 }}
                    />
                    <DarkColoredText size="12px" numberOfLines={2} style={{ width: "80%" }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    </DarkColoredText>
                    <MaterialCommunityIcons name="chevron-down" size={CONSTANTS.NORMAL_ICON_SIZE - 10} color={theme.secondaryColor} />
                </RowLayout>
            </SinglePinnedCommentContainer>
        </TouchableWithoutFeedback>
    )
}

export default SinglePinnedComment