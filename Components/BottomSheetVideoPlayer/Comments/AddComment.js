import moment from 'moment';
import React, { useEffect, useRef, useState } from 'react';
import { Text, View, Image, TouchableOpacity, TextInput, TouchableWithoutFeedback } from 'react-native';


import { useSelector } from 'react-redux'
// import { getUserProfileFromDetailObject } from '../../../Utils';

import { Assets, theme } from '../../config';
// import { addVideoComment } from '../../../Apis/Node/InstituteVideoComments';

import { Input, RowLayout } from '../../GlobalStyle';
import { Ionicons } from '@expo/vector-icons';
import { imageProvider } from '../../config';
import ImageViewExtension from '../../Utils/ImageViewExtension/ImageViewExtension';
import { addVideoComment } from '../../Utils/DataHelper/CourseVideos';

const DEFAULT_COMMENT_BOX_HEIGHT = 40;
const MAX_COMMENT_BOX_HEIGHT = 65;
const AddComment = (props) => {
    const item = props.item;
    const mode = props.mode;
    const autoFocus = props.autoFocus;
    const [comment, setComment] = useState("")
    const inputRef = useRef(null);
    const [isTextInputFocused, setIsTextInputFocused] = useState(false)
    // * to adjust height of text input according to content of input field
    const [textInputHeight, setTextInputHeight] = useState(0)
    
    const userDetails = useSelector(state =>state.user.userInfo) 
    const userProfile = userDetails 
    const addCommentHandler =  () => {
        addVideoComment(comment, userDetails.id, props.videoId,addCommentCallback);
        // addCommentCallback(response)
    }
    const addCommentCallback = (response) => {
        if (response.status==201) {
            // console.log("comment added");
            props.unshiftCommets({ comment, commentTime: Date.now(), student: userDetails, videoId: props.videoId })
            setComment("")
        }
    }

    useEffect(() => {
        if (autoFocus) {
            setInputFocused()
        }
    }, [autoFocus])

    const setInputFocused = () => {
        if (inputRef.current) {
            inputRef.current.focus()
        } else {
            setTimeout(() => {
                setInputFocused()
            }, 100);
        }

    }
    return (
        <RowLayout style={{ padding: 10, marginBottom: 10, borderTopWidth: 0.8, borderTopColor: theme.labelOrInactiveColor }}>
            <ImageViewExtension
                source={{uri:imageProvider(userProfile?.studentImage)}}
                style={{
                    height: 30,
                    width: 30,
                    borderRadius: 20,
                    marginRight: 10,
                    alignSelf: (isTextInputFocused && textInputHeight > DEFAULT_COMMENT_BOX_HEIGHT) ? "flex-start" : "center",
                }}
                errorImage={Assets.profile.profileIcon}
            />
            <Input
                ref={inputRef}
                onFocus={() =>{
                    props.setIsInputFocused(true)
                    setIsTextInputFocused(true)
                }}
                onBlur={() => {
                    props.setIsInputFocused(false)
                    setIsTextInputFocused(false)
                }}
                onChangeText={(text) => setComment(text)}
                defaultValue={comment}
                onLayout={()=> {
                    if(autoFocus)
                    {
                        inputRef.current.focus()
                    } 
                }}
                style={{ width: "80%" }}
                height={isTextInputFocused ? Math.min(Math.max(DEFAULT_COMMENT_BOX_HEIGHT, textInputHeight), MAX_COMMENT_BOX_HEIGHT) : DEFAULT_COMMENT_BOX_HEIGHT + 'px'}
                placeholder="Add a public comment..."
                multiline
                autoFocus={autoFocus}
                borderWidth={'0px'}
                borderRadius={'5px'}
                onContentSizeChange={(e) => {
                    setTextInputHeight(e.nativeEvent.contentSize.height)
                }}
                backgroundColor={theme.silverColor + "44"}
            />
            {comment.length ? (
                <View style={[{ marginHorizontal: 10, }, (isTextInputFocused && textInputHeight > DEFAULT_COMMENT_BOX_HEIGHT) ? { alignSelf: "flex-end", marginBottom: 10 } : { alignSelf: "center",marginBottom: 0 }]}>
                    <TouchableOpacity activeOpacity={0.8} onPress={addCommentHandler}>
                        <View>
                            <Ionicons name="send" size={20} color={theme.greyColor} />
                        </View>
                    </TouchableOpacity>
                </View>) : (null)}
        </RowLayout>

    )
}

export default AddComment;
