import { View, FlatList, Text, TouchableOpacity, TouchableWithoutFeedback, Animated } from 'react-native'
import React, { useEffect, useRef, useState } from 'react'
import AddComment from './AddComment'
import VideoCommentItem from './VideoCommentItem'

// import { fetchVideoComments } from '../../Apis/Node/InstituteVideoComments'
import { CONSTANTS, dataLimit } from '../../config'
import { BoldDarkColoredText, ColoredUnderline, DarkColoredText, RowLayout, SectionContent, SectionView } from '../../GlobalStyle'
import { CommunityGuidelinesNoticeContainer, DarkTranslucentOverlay, VideoCommentFilterButton } from './Style'
import { MaterialIcons } from '@expo/vector-icons'
import { theme } from '../../config'
import { fetchVideoComments } from '../../Utils/DataHelper/CourseVideos'
import { useSelector } from 'react-redux'

function Comments({ videoId, mode, autoFocus,setIsCommentsVisible, setIsSideScreenVisible,closeComments,   showCloseIcon,panHandlers }) {
    
    const keyboardHeight = useSelector(state=>state.screen.keyboardHeight)
    // console.log("keyboardHeight",keyboardHeight)
    const [comments, setComments] = useState([])
    const [isInputFocused, setIsInputFocused] = useState(autoFocus)
    
    const unshiftCommets = (comment) => {
        setComments([comment, ...comments])
    }
    
    const [offset, setOffset] = useState(0)
    useEffect(() => {
        getVideoComments()
    }, [offset])
    const getVideoComments = async () => {
        fetchVideoComments(videoId, offset, dataLimit,(response) => { 
            if (response.status == 200) {
                response.json().then(data => { 
                    if (offset == 0) {
                        setComments(data);
                    } else {
                        setComments([...comments, ...data])
                    }
                })
            }
        }) 
    }
    useEffect(() => {
        if (isInputFocused&&!keyboardHeight) {
            setIsInputFocused(false)
        }
    }, [keyboardHeight])
 
    return (
        <View style={{flex:1}}>
            <SectionView marginTop="0px"
                {...panHandlers}
            >
                <SectionContent>
                    <RowLayout style={{justifyContent:"space-between"}}> 
                        <BoldDarkColoredText   size="20px">Comments</BoldDarkColoredText>
                        <TouchableWithoutFeedback onPress={closeComments}>
                                <MaterialIcons name="close" size={CONSTANTS.NORMAL_ICON_SIZE} color={theme.secondaryColor} />
                            </TouchableWithoutFeedback>
                    </RowLayout> 
                </SectionContent>
            </SectionView>
            
            <View style={{ flex: 1 }}>
                <View style={{ flex: 0.85 }}>
                    <FlatList
                        data={comments}
                        ListHeaderComponent={ 
                            <>
                                {/* <RowLayout style={{ marginTop: 10 }}>
                                    <VideoCommentFilterButton style={{ marginHorizontal: 5 }} active={true}>
                                        <DarkColoredText color={theme.primaryColor} size="14px">Top</DarkColoredText>
                                    </VideoCommentFilterButton>
                                    <VideoCommentFilterButton style={{ marginHorizontal: 5 }}>
                                        <DarkColoredText color={theme.secondaryColor} size="14px">Newest</DarkColoredText>
                                    </VideoCommentFilterButton>
                                </RowLayout> */}
                                <CommunityGuidelinesNoticeContainer>
                                    <DarkColoredText size="13px" > 
                                        <TouchableOpacity>
                                            <DarkColoredText  size="13px" >Remember to keep your comments respectful and follow our</DarkColoredText>
                                        </TouchableOpacity>
                                        <TouchableOpacity>
                                            <DarkColoredText  color={theme.secondaryColor} size="13px" >Community guidelines.</DarkColoredText>
                                            <ColoredUnderline color={theme.darkPurpleColor} height="1px" />
                                        </TouchableOpacity>
                                    </DarkColoredText> 
                                </CommunityGuidelinesNoticeContainer>
                            </>
                        }
                        renderItem={({ item }) => <VideoCommentItem mode={mode} item={item} />}
                        keyExtractor={(item) => item.id}
                        showsVerticalScrollIndicator={false}
                    />  
                </View>
                {/* {isInputFocused?(
                    <DarkTranslucentOverlay/>
                ):null} */}
                <View style={[{ flex:0.15 },(isInputFocused)?{position:"absolute",bottom: Math.max(CONSTANTS.DEFAULT_KEYBOARD_HEIGHT,keyboardHeight),backgroundColor:theme.primaryColor}:{}]}>
                    <AddComment mode={mode} autoFocus={isInputFocused} setIsInputFocused={setIsInputFocused} videoId={videoId} unshiftCommets={unshiftCommets} />
                </View>
            </View>
        </View>
    )
}

export default Comments
