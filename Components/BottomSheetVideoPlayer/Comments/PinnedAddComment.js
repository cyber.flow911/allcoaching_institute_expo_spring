import { View, Text } from 'react-native'
import React from 'react'
import { SinglePinnedCommentContainer, VideoCommentInputButton } from './Style'
import { useSelector } from 'react-redux' 
import { DarkColoredText, RowLayout } from '../../GlobalStyle'
import ImageViewExtension from '../../Utils/ImageViewExtension/ImageViewExtension'
import { Assets, imageProvider } from '../../config'

const PinnedAddComment = ({onPress}) => {
        const userDetails = useSelector(state =>state.user.userInfo) 
        const userProfile = userDetails 
    
    return (
        <SinglePinnedCommentContainer style={{paddingBottom:20}}>
            <RowLayout style={{marginBottom:10}}>
                <DarkColoredText  size="13px" fontFamily="Raleway_600SemiBold" style={{marginRight:10}}>
                    Add Comment
                </DarkColoredText> 
            </RowLayout>
            <RowLayout>
                <ImageViewExtension
                    source={{uri:imageProvider(userProfile?.studentImage)}}
                    errorImage={Assets.profile.profileIcon}
                    style={{height:30, width: 30, borderRadius: 20,marginRight:10}}
                />
                <VideoCommentInputButton
                    onPress={onPress}
                >
                    <DarkColoredText size="12px" numberOfLines={2} style={{width:"80%"}}>
                        Add a comment...
                    </DarkColoredText>
                </VideoCommentInputButton>

            </RowLayout>
        </SinglePinnedCommentContainer>
    )
}

export default PinnedAddComment