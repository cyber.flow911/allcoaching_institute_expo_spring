import styled from 'styled-components/native'
import { theme } from './config'
import { Dimensions, Platform } from 'react-native';

import { Card } from 'react-native-shadow-cards'
const width = Dimensions.get('window').width;
export const HeaderTitleText = styled.Text`
    font-family: Raleway_700Bold;
    font-size:20px;
    color:${theme.primaryColor};
    margin-left:10px;
    margin-right:5px;
    flex:1;

`

export const OnBoardScreenHeading = styled.Text`

font-family: Raleway_500Medium;
font-style: normal;
font-weight: 400;
font-size: 30px;
color: ${theme.secondaryColor};
`
// *style for subheading or second line of text  
export const SecondaryText = styled.Text`
font-family: Raleway_400Regular;
font-style: normal;
font-weight: 400;
font-size: 13px; 
text-align: center;
color: ${props => props.color || theme.accentColor};`

export const InputBox = styled.TextInput`
border: 1.5px solid ${props => props.borderColor || theme.labelOrInactiveColor};
font-family: Raleway_500Medium;
width: ${props => props.width + "px" || '100%'};
border-radius: ${props => props.borderRadius || '10px'};
padding: ${props => props.padding || '10px'};
padding-left:${props => props.paddingLeft || '30px'}; 
`
export const InputLabel = styled.Text`
    font-size:${props => props.fontSize || '20px'};
    font-family: Raleway_500Medium;
    font-style: normal;
    font-weight: 400; 
    color: ${props => props.color || theme.secondaryColor};
`
export const BorderedBoxView = styled.View`
border: ${props => props.borderWidth || '1.5px'} solid ${props => props.borderColor || theme.labelOrInactiveColor}; 
width: ${props => props.width || '100%'};
padding: ${props => props.padding || '10px'};
border-radius: 10px;
background-color: ${props => props.backgroundColor || theme.primaryColor};
`
export const DashedBorderedBoxView = styled.View`
border: ${props => props.borderWidth || '1.5px'} dashed ${props => props.borderColor || theme.labelOrInactiveColor}; 
width: ${props => props.width || '100%'};
padding: ${props => props.padding || '10px'};
border-radius: 10px;
background-color: ${props => props.backgroundColor || theme.primaryColor};
`

export const BottomBorderView = styled.View`
    flex-direction: ${props => props.flexDirection || 'row'};
    align-items: ${props => props.alignItems || 'center'};
    justify-content: ${props => props.justifyContent || 'center'};
    border-bottom-width: 1px;
    border-right-width: .5px;
    border-left-width: .5px;
    border-color: ${props => props.borderColor || theme.labelOrInactiveColor};
`
export const FilledButtonView = styled.View`
background: ${props => props.fillColor || theme.secondaryColor};
height:${props => props.height || '60px'};
width: ${props => props.width || '100%'};
border-radius: ${props => props.borderRadius || '16px'};
align-items: center;
justify-content: center;`

// export const ButtonText = styled.Text`
// font-family: 'DM Sans';
// font-style: normal;
// font-weight: 500;
// font-size: 24px;
// color: ${theme.guestColors.guestColor8};`


export const RadioButtonView = styled.View`
background: ${props => props.checked ? theme.secondaryColor : theme.primaryColor};
border-radius: ${props => props.borderRadius || '10px'}; 
border: 1.5px solid ${props => props.checked ? theme.secondaryColor : theme.labelOrInactiveColor};
padding: 5px 10px 5px 10px;
align-items: center;
justify-content: center;
display: flex;
`
export const RadioButtonText = styled.Text`
font-family: Raleway_500Medium;
font-style: normal;
font-weight: 400;
font-size: ${props => props.fontSize || '16px'}; 

color: ${props => props.checked ? theme.primaryColor : theme.btn_dark_background_color};
`

export const ScreenContainer = styled.View`
flex: 1;
 background-color: ${props => props.backgroundColor || theme.appBackgroundColor};
`
export const VerticalSeprator = styled.View`
    width: ${props => props.width || '1px'};
    height: ${props => props.height || '70%'};
    border-radius: 10px;
    background-color: ${props => props.color || theme.accentColor};
    border: 0.5px solid ${props => props.color || theme.accentColor};
`


export const HorizontalCenterView = styled.View`
    display: flex;
    flex-direction: row; 
    justify-content: center;
`
export const SectionView = styled.View`
    margin-top: ${props => props.marginTop || '10px'};
    margin-bottom: ${props => props.marginBottom || '5px'};
    margin-left: 10px;
    margin-right: 10px;


`

export const SectionHeadingText = styled.Text`
    font-family: Raleway_700Bold; 
    font-size: 18px;
    color: ${theme.secondaryColor};
`

export const DarkColoredText = styled.Text`
    font-family: ${props => props.fontFamily || "Raleway_500Medium"};
    font-style: normal;
    font-weight: 500;
    font-size: ${props => props.size || '15px'};
    color: ${props => props.color || theme.secondaryColor};

`
export const BoldDarkColoredText = styled.Text`
    font-family: Raleway_700Bold;
    font-size: ${props => props.size || '15px'};
    color: ${props => props.color || theme.secondaryColor};

`
export const AbsoluteTopView = styled.View`
    position: absolute;
    top: 5px;
    left: 5px; 
    z-index: 1;
    width: ${width}px;
    
`
export const SectionContent = styled.View`
    margin-top: ${props => props.marginTop || '5px'};
    margin-bottom: ${props => props.marginBottom || '5px'};

`

export const RowLayout = styled.View`
    flex-direction: row;
    align-items: ${props => props.normalRow ? 'flex-start' : 'center'};
`
export const WidthResponsiveRowLayout = styled.View`
    flex-direction: row;
    align-items: ${props => props.normalRow ? 'flex-start' : 'center'};
    width:${props => props.width || width}px;
    flex-wrap:wrap;
`

export const ColoredUnderline = styled.View`
    background-color: ${props => props.color || theme.buttonColor};
    width: ${props => props.width || '90%'};
    margin-top: 2px;
    height: ${props => props.height || '1px'}; 
    border-radius: ${props => props.borderRadius || '5px'};
    margin-bottom: ${props => props.marginBottom || '2px'};

`

export const ButtonView = styled.View`
    background-color: ${props => props.backgroundColor || theme.secondaryColor};
    border-radius: ${props => props.borderRadius || '8px'}; 
    border-color: ${props => props.borderColor || theme.secondaryColor};
    border-width: ${props => props.borderWidth || '1px'};
    align-items: center;
    justify-content: center;
    flex-direction: row;
    padding-left: ${props => props.paddingLeft || '10px'};
    padding-right: ${props => props.paddingRight || '10px'};
    padding-top: ${props => props.paddingTop || '12px'};
    padding-bottom: ${props => props.paddingBottom || '12px'};
    border-style: ${props => props.borderStyle || 'solid'};

`

export const ButtonText = styled.Text`
    font-family: Raleway_600SemiBold;
    font-size: ${props => props.size || '16px'};
    color: ${props => props.color || theme.primaryColor};
    

`
export const ColoredBackgroundView = styled.View`
    background-color: ${props => props.backgroundColor || theme.secondaryColor + "66"};
    border-radius: ${props => props.borderRadius || '20px'};
    border-color: ${props => props.borderColor || theme.secondaryColor + "66"};
    border-width: ${props => props.borderWidth || '1px'};
    align-items: center;
    justify-content: center;

`




export const UserPlanView = styled.View`    
    border-radius: 10px;
    background-color: ${props => props.backgroundColor || theme.accentColor};
    padding-left: 10px;
    padding-right: 10px;
    border-width: 1px;
    align-items: center;
    border-color: ${props => props.borderColor || theme.primaryColor};


`
export const UserPlanViewText = styled.Text`
    font-family: Raleway_500Medium;
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    color: ${props => props.color || theme.primaryColor};

`

export const Input = styled.TextInput`
    font-family: Raleway_500Medium;
    font-style: normal;
    font-weight: 500;
    height:${props => props.height || '50px'};
    font-size: 15px;
    color: ${props => props.color || theme.secondaryColor};
    background-color: ${props => props.backgroundColor || theme.purpleColor};
    border-radius: ${props => props.borderRadius || '10px'};
    border-color: ${props => props.borderColor || theme.labelOrInactiveColor};
    border-width: ${props => props.borderWidth || '1px'};   
    padding:5px;
`



export const ModalContainer = styled.View`
    background-color: ${props => props.backgroundColor || theme.secondaryColor + "AA"};
    flex: 1;
    justify-content: center;
    align-items: center;
    padding-top:${Platform.OS == "ios" ? 35 : 0}px    
    

`

export const ModalContent = styled.View`
    background-color: ${props => props.backgroundColor || theme.primaryColor};
    width: ${props => props.width || (width - 40) + 'px'};
    border-radius: ${props => props.borderRadius || '5px'};   
    align-items: center;
    padding:10px
    justify-content: center;

`

export const ModalImage = styled.Image`
    width: ${props => props.width || '50%'};
    height: ${props => props.height || '40%'};
    resize-mode: contain;
    margin: 10px
`



export const IconBackgroundView = styled.View`
    background-color: ${props => props.backgroundColor || theme.primaryColor + "99"};
    border-radius: ${props => props.borderRadius || '10px'};
    width: ${props => props.width || '30px'};
    height: ${props => props.height || '30px'};
    justify-content: center;
    align-items: center;

`

export const CardContainer = styled.View`
    background-color: ${props => props.backgroundColor || theme.labelOrInactiveColor + (Platform.OS == "ios" ? '2B' : '33')};
    border-radius: ${props => props.borderRadius || '10px'};
    padding: 5px;
`

export const FloatingView = styled(Card)`
width: ${props => props.width || '60px'};
height: ${props => props.height || '60px'};
borderRadius: ${props => props.borderRadius || '30px'};            
backgroundColor: ${props => props.backgroundColor || theme.secondaryColor};
position: absolute;
bottom: ${props => props.bottom || '20px'};                                                    
right: ${props => props.right || '20px'};
overflow: hidden;
alignItems: center;
justifyContent: center;
zIndex: ${props => props.zIndex || '1'}; 
`
export const SmallText = styled.Text`
    font-family: ${props => props.fontFamily || 'Raleway_400Regular'};
    font-style: normal; 
    font-size: ${props => props.fontSize || '12px'};
    color: ${props => props.color || theme.greyColor};

`
export const FloatingRow = styled(Card)`
width: ${props => props.width || '60px'};
borderRadius: ${props => props.borderRadius || '30px'};            
backgroundColor: ${props => props.backgroundColor || theme.secondaryColor};
position: absolute;
bottom: ${props => props.bottom || '20px'};                                                    
right: ${props => props.right || '20px'};
overflow: hidden;
alignItems: center;
justifyContent: center;
zIndex: ${props => props.zIndex || '1'};    
`

export const PickerInputView = styled.View`
    font-family: Raleway_500Medium;
    font-style: normal;
    font-weight: 500;
    font-size: 15px;
    color: ${props => props.color || theme.secondaryColor};
    background-color: ${props => props.backgroundColor || theme.purpleColor};
    border-radius: ${props => props.borderRadius || '10px'};
    border-color: ${props => props.borderColor || theme.labelOrInactiveColor};
    border-width: ${props => props.borderWidth || '1px'};
    overflow: hidden;

`
export const Hr = styled.View`
    width:100%;
    height:${props => props.height || '1px'};
    background-color:${props => props.color || theme.labelOrInactiveColor};
`

