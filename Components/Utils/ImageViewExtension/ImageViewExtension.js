import { View, Text,Image, Platform } from 'react-native'
import React from 'react'
import { useRef } from 'react'

const ImageViewExtension = ({ onError,errorImage, ...rest }) => {
  const imageRef = useRef()
  const onErrorHandler = () => {
          if(imageRef.current&&errorImage)
          { 
              imageRef.current.setNativeProps({
                  src:[Image.resolveAssetSource(errorImage)]
              })
          }
      
  }
  const defaultSourceProps = Platform.OS === 'android' ? { onError: onErrorHandler } : {defaultSource:errorImage}
  return (
      <Image 
        ref={imageRef} 
        {...defaultSourceProps}
        {...rest}
      />
  )
}

export default ImageViewExtension