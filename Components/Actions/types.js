/* 
        define and export all the action types here and use them in actions as well as in reducers

        ==> export const ACTION_TYPE = "ACTION_TYPE"


*/

export const SET_CATEGORIES= "SET_CATEGORIES"; 
export const  USER_AUTH_STATUS = "USER_AUTH_STATUS";
export const SCREEN_WIDTH_CONFIG_CHANGE = "SCREEN_WIDTH_CONFIG_CHANGE";
export const SET_USER_INFO="SET_USER_INFO";
export const SET_INSTITUTE_AUTH="SET_INSTITUTE_AUTH"; 
export const SET_TEST_RESULT_DATA="SET_TEST_RESULT_DATA"; 
export const SET_NAVIGATION='SET_NAVIGATION';
export const SET_STATUS_BAR_HIDDEN='SET_STATUS_BAR_HIDDEN';
export const SET_KEYBOARD_HEIGHT='SET_KEYBOARD_HEIGHT';
export const SET_ACTIVE_BOTTOM_TAB='SET_ACTIVE_BOTTOM_TAB';
export const SET_DOWNLOADING_ITEM='SET_DOWNLOADING_ITEM';
export const SET_DOWNLOADING_PROGRESS='SET_DOWNLOADING_PROGRESS';
export const REMOVE_DOWNLOADING_ITEM="REMOVE_DOWNLOADING_ITEM";
export const SET_PINNED_INSTITUTE="SET_PINNED_INSTITUTE"
export const SET_PINNED_INSTITUTE_COUNT="SET_PINNED_INSTITUTE_COUNT"
export const SET_HEADER_PROPS="SET_HEADER_PROPS"
export const TOGGLE_HEADER="TOGGLE_HEADER"
export const SHOW_CATEGORIES_IN_HEADER="SHOW_CATEGORIES_IN_HEADER"
export const SET_HEADER_OFFSET="SET_HEADER_OFFSET"

export const TOGGLE_VIDEO_PLAYER_MOUNT_STATE="TOGGLE_VIDEO_PLAYER_MOUNT_STATE"
export const SET_VIDEO_PLAYER_DATA="SET_VIDEO_PLAYER_DATA"
export const CLEAR_VIDEO_PLAYER_DATA = "CLEAR_VIDEO_PLAYER_DATA"
export const TOGGLE_VIDEO_PLAYER_OPEN_IN_MINIMIZED_STATE = "TOGGLE_VIDEO_PLAYER_OPEN_IN_MINIMIZED_STATE"



export const SET_INSTITUTE_COURSE_VIDEOS="SET_INSTITUTE_COURSE_VIDEOS"
export const SET_INSTITUTE_COURSE_LIVE_VIDEOS ="SET_INSTITUTE_COURSE_LIVE_VIDEOS"
export const SET_ACTIVE_COURSE_DETAILS = "SET_ACTIVE_COURSE_DETAILS"
export const SET_INSTITUTE_COURSE_VIDEO_PLAYLISTS = "SET_INSTITUTE_COURSE_VIDEO_PLAYLISTS"
export const SET_COURSE_ACTIVE_VIDEO_PLAYLIST = "SET_COURSE_ACTIVE_VIDEO_PLAYLIST"
export const SET_USER_ENROLLED_COURSES = "SET_USER_ENROLLED_COURSES"
export const SET_INSTITUTE_DETAILS = "SET_INSTITUTE_DETAILS" 

export const SET_IS_TAB_BAR_VISIBLE = "SET_IS_TAB_BAR_VISIBLE"